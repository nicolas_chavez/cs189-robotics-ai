
'''
face detection using haar cascades
USAGE:
    facedetect.py [--cascade <cascade_fn>] [--nested-cascade <cascade_fn>] [<video_source>]
'''

import numpy as np
import cv2
import rospy
from SensorManagers.camera_manager import CameraManager


class FaceDetector:

    def __init__(self):

        root = '/home/turtlebot/Desktop/Triton/FinalProject/Brainz/ObjectDetectors/FaceDetector/'
        path = root + 'haarcascade_frontalface_alt.xml'
        self.face_cascade = cv2.CascadeClassifier(path)
        self.debug_image = None

    def detect(self, image):

        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        gray = cv2.equalizeHist(gray)

        rects = self.detect_face(gray, image)

        return rects

    def detect_face(self, gray, image):

        rects = self.face_cascade.detectMultiScale(
            gray, scaleFactor=1.3, minNeighbors=4, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)

        if len(rects) != 0:
            rects[:, 2:] += rects[:, :2]
            self.draw_rects(image, rects, (0, 255, 0))

        image = cv2.resize(image, (0, 0), fx=0.5, fy=0.5)
        self.debug_image = image

        return self.convert_rects(rects)

    @staticmethod
    def convert_rects(rects):

        new_rects = []
        for x1, y1, x2, y2 in rects:
            w = x2-x1
            h = y2-y1
            new_rects.append((x1, y1, w, h))
        return new_rects

    @staticmethod
    def draw_rects(image, rects, color):

        for x1, y1, x2, y2 in rects:
            cv2.rectangle(image, (x1, y1), (x2, y2), color, thickness=4)

if __name__ == '__main__':

    import time
    rospy.init_node('FaceDetectTest', anonymous=False)
    cm = CameraManager(source=CameraManager.CameraSource.WebcamBottom)
    cm.start()
    fd = FaceDetector()

    while True:

        if cm.current_image is not None:

            img = np.copy(cm.current_image)
            img = cv2.resize(img, (0, 0), fx=0.66667, fy=0.66667)
            _ = fd.detect(img)

            if fd.debug_image is not None:
                cv2.imshow('Face', fd.debug_image)
                cv2.waitKey(3)

            time.sleep(0.1)
