import cv2
import numpy as np
import rospy
import time

from SensorManagers.camera_manager import CameraManager
from object_detector import ObjectDetector

# Searches for cube in images


class HomeDetector(ObjectDetector):

    def __init__(self):

        ObjectDetector.__init__(self)

        # Set size and aspect ratio values
        self.size_threshold = 450
        self.aspect_ratio_lb = 0.0  # 0.7
        self.aspect_ratio_ideal = 0.3  # 1.1
        self.aspect_ratio_ub = 0.45  # 2
        self.val_lb = 0  # 40
        self.val_ub = 255  # 220

        self.adaptive_padding_x = 30
        self.adaptive_padding_y = 30

    # Threshold by coloring
    def color_threshold(self, query_im):

        im_h, im_w, _ = query_im.shape

        query_im = cv2.GaussianBlur(query_im, (5, 5), 1)

        query_hsv = cv2.cvtColor(query_im, cv2.COLOR_BGR2HSV)

        # Acceptable colorings
        # Hue: (73, 120)
        # Sat: (127, 240)
        # Val: (33, 182)

        # lower_green = np.array([73, 0, 0])
        # upper_green = np.array([120, 255, 255])
        lower_green = np.array([106, 0, 0])
        upper_green = np.array([167, 255, 255])
        green_mask = cv2.inRange(query_hsv, lower_green, upper_green)

        # Clean up noise
        kernel = np.ones((3, 3), np.uint8)
        green_mask = cv2.morphologyEx(green_mask, cv2.MORPH_ERODE, kernel)

        kernel = np.ones((7, 7), np.uint8)
        green_mask = cv2.morphologyEx(green_mask, cv2.MORPH_DILATE, kernel)

        green_thresh = cv2.bitwise_and(query_hsv, query_hsv, mask=green_mask)
        green_thresh = cv2.cvtColor(green_thresh, cv2.COLOR_HSV2BGR)

        # Find blobs!
        contours, _ = cv2.findContours(green_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        bounding_rectangle = None

        for contour in contours:

            # Approximate bounding boxes
            poly = cv2.approxPolyDP(contour, 3, True)
            x, y, w, h = cv2.boundingRect(poly)
            ar = float(w)/float(h)

            # Make sure rectangle big enough and in lower 2/3 of image
            if w*h > self.size_threshold and self.aspect_ratio_ub >= ar >= self.aspect_ratio_lb:

                # Take the biggest rectangle so far!
                # And better aspect ratio score

                if bounding_rectangle is None:
                    bounding_rectangle = (x, y, w, h)
                else:
                    _, _, old_w, old_h = bounding_rectangle

                    ar_score = self.aspect_ratio_score(w, h)
                    old_ar_score = self.aspect_ratio_score(old_w, old_h)

                    if w*h > old_w*old_h and ar_score >= old_ar_score:
                        bounding_rectangle = (x, y, w, h)

        if bounding_rectangle is not None:

            x, y, w, h = bounding_rectangle
            cv2.rectangle(green_thresh, (x, y), (x + w, y + h), (255, 0, 0), thickness=6)

        """
        cv2.drawContours(green_thresh, contours, contourIdx=-1, color=(0, 0, 255), thickness=5)

        cv2.imshow('Thresh', green_thresh)
        cv2.waitKey(3)
        """

        return green_thresh, bounding_rectangle

    def estimate_distance(self, image_size, bounding_rectangle):

        # 0.2m, 74
        # 0.5m, 51
        # 1.0m, 31
        # 2.0m, 16
        # 3.0m, 10

        # 116, 334 (all h)

        _, _, w, _ = bounding_rectangle

        distance = 70.044*(w**-1.299)

        # 70.044x-1.299

        return distance

    def estimate_angle(self, image_size, bounding_rectangle):

        # 1m, 0.0m, 189px -> 0deg
        # 1m, -0.3m, 280px -> -17 deg
        # 1m, 0.3m, 84px -> 17 deg

        im_w, im_h = image_size
        x, _, w, _ = bounding_rectangle

        # y = -0.1732x + 31.922

        center_px = (x + w/2)

        angle = -0.1732*center_px + 31.922

        return angle

    # Kickoff detection
    def detect(self, query_im):

        _, color_rect = self.color_threshold(query_im)

        bounding_rectangle = color_rect

        # estimate distance
        h, w, _ = query_im.shape
        if bounding_rectangle is not None:
            est_distance = self.estimate_distance((w, h), bounding_rectangle)
            est_angle = self.estimate_angle((w, h), bounding_rectangle)
        else:
            est_distance, est_angle = None, None

        return bounding_rectangle, est_distance, est_angle

    @staticmethod
    def render_detection(query_im, rectangle):

        if rectangle is not None:

            x, y, w, h = rectangle

            cv2.rectangle(query_im, (x, y), (x+w, y+h), (255, 0, 200), thickness=2)

        cv2.imshow('HomeDetector', query_im)
        cv2.waitKey(3)

if __name__ == "__main__":

    # if running standalone

    rospy.init_node('ThresholdGUI', anonymous=False)
    cm = CameraManager(source=CameraManager.CameraSource.Asus)
    cm.start()

    hd = HomeDetector()

    while True:

        if cm.current_image is not None:

            im = np.copy(cm.current_image)

            # im = cv2.resize(im, (0, 0), fx=0.3, fy=0.3)
            """
            im = cv2.transpose(im)
            im = cv2.flip(im, 0)
            """

            rect, dist, ang = hd.detect(im)
            HomeDetector.render_detection(im, rect)
            time.sleep(0.05)

