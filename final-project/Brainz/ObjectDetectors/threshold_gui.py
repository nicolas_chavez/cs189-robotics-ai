
import cv2
import numpy as np
import time
import rospy

from SensorManagers.camera_manager import CameraManager


class ThresholdGUI:

    def __init__(self, im_scale=1.0):

        self.hue = (0, 180)
        self.sat = (0, 255)
        self.val = (0, 255)

        self.im_scale = im_scale

        self.window_name = 'Threshold GUI'

    def nada(self, x):
        pass

    def create_window(self):

        cv2.namedWindow(self.window_name)

        # create trackbars for color change
        cv2.createTrackbar('Hue Min', self.window_name, 0, 180, self.nada)
        cv2.createTrackbar('Sat Min', self.window_name, 0, 255, self.nada)
        cv2.createTrackbar('Val Min', self.window_name, 0, 255, self.nada)

        cv2.createTrackbar('Hue Max', self.window_name, 0, 180, self.nada)
        cv2.createTrackbar('Sat Max', self.window_name, 0, 255, self.nada)
        cv2.createTrackbar('Val Max', self.window_name, 0, 255, self.nada)

    def read_values(self):

        h_min = cv2.getTrackbarPos('Hue Min', self.window_name)
        h_max = cv2.getTrackbarPos('Hue Max', self.window_name)
        s_min = cv2.getTrackbarPos('Sat Min', self.window_name)
        s_max = cv2.getTrackbarPos('Sat Max', self.window_name)
        v_min = cv2.getTrackbarPos('Val Min', self.window_name)
        v_max = cv2.getTrackbarPos('Val Max', self.window_name)

        if h_min > h_max:
            h_min = h_max
        if s_min > s_max:
            s_min = s_max
        if v_min > v_max:
            v_min = v_max

        self.hue = (h_min, h_max)
        self.sat = (s_min, s_max)
        self.val = (v_min, v_max)

    def render(self, image, wait_key_time=3):

        self.read_values()

        image_hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

        lower_color = np.array([self.hue[0], self.sat[0], self.val[0]])
        upper_color = np.array([self.hue[1], self.sat[1], self.val[1]])

        range_mask = cv2.inRange(image_hsv, lower_color, upper_color)

        thresh_hsv = cv2.bitwise_and(image_hsv, image_hsv, mask=range_mask)

        thresh = cv2.cvtColor(thresh_hsv, cv2.COLOR_HSV2BGR)

        thresh_resize = cv2.resize(thresh, (0, 0), fx=self.im_scale, fy=self.im_scale)
        cv2.imshow(self.window_name, thresh_resize)
        cv2.waitKey(wait_key_time)

if __name__ == "__main__":

    rospy.init_node('ThresholdGUI', anonymous=False)
    cm = CameraManager(source=CameraManager.CameraSource.WebcamBottom)
    cm.start()

    tg = ThresholdGUI(im_scale=0.3)
    tg.create_window()

    while True:

        if cm.current_image is not None:

            tg.render(cm.current_image)
            time.sleep(0.05)
