import cv2
import numpy as np
import rospy
import time

from SensorManagers.camera_manager import CameraManager
from object_detector import ObjectDetector

# Searches for cube in images


class TrayDetector(ObjectDetector):

    def __init__(self):

        ObjectDetector.__init__(self)

        # Set size and aspect ratio values
        self.size_threshold = 450
        self.aspect_ratio_lb = 0.0  # 0.7
        self.aspect_ratio_ideal = 0.3  # 1.1
        self.aspect_ratio_ub = 0.45  # 2
        self.val_lb = 0  # 40
        self.val_ub = 255  # 220

        self.adaptive_padding_x = 30
        self.adaptive_padding_y = 30

    # Threshold by coloring
    def color_threshold(self, query_im):

        im_h, im_w, _ = query_im.shape

        query_im = cv2.GaussianBlur(query_im, (5, 5), 1)

        query_hsv = cv2.cvtColor(query_im, cv2.COLOR_BGR2HSV)


        lower_green = np.array([44, 44, 0])
        upper_green = np.array([88, 145, 255])
        green_mask = cv2.inRange(query_hsv, lower_green, upper_green)

        # Clean up noise
        kernel = np.ones((3, 3), np.uint8)
        green_mask = cv2.morphologyEx(green_mask, cv2.MORPH_ERODE, kernel)

        kernel = np.ones((7, 7), np.uint8)
        green_mask = cv2.morphologyEx(green_mask, cv2.MORPH_DILATE, kernel)

        green_thresh = cv2.bitwise_and(query_hsv, query_hsv, mask=green_mask)
        green_thresh = cv2.cvtColor(green_thresh, cv2.COLOR_HSV2BGR)

        # Find blobs!
        contours, _ = cv2.findContours(green_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        return green_thresh, contours

    def count_blobs(self, query_im):

        _, contours = self.color_threshold(query_im)

        num = len(contours)
        return num

    @staticmethod
    def render_detection(query_im, green_thresh, contours):

        cv2.drawContours(green_thresh, contours, contourIdx=-1, color=(255, 0, 255), thickness=5)

        cv2.imshow('TrayDetector', query_im)
        cv2.waitKey(3)

if __name__ == "__main__":

    # if running standalone

    rospy.init_node('ThresholdGUI', anonymous=False)
    cm = CameraManager(source=CameraManager.CameraSource.WebcamBottom)
    cm.start()

    td = TrayDetector()

    while True:

        if cm.current_image is not None:

            im = np.copy(cm.current_image)

            im = cv2.resize(im, (0, 0), fx=0.3, fy=0.3)

            # im = im[:,180:423]
            """
            im = cv2.transpose(im)
            im = cv2.flip(im, 0)
            """

            num = td.count_blobs(im)

            #green_thresh, contours = td.color_threshold(im)
            #td.render_detection(im, green_thresh, contours)

            print num
            time.sleep(0.05)

