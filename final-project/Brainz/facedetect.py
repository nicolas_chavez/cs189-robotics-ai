#!/usr/bin/env python

import time

import numpy as np
import cv2

import rospy
from SensorManagers.camera_manager import CameraManager


def draw_detections(image, gray, faces, thickness = 1):
    
    eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')

    for (x,y,w,h) in faces:
        cv2.rectangle(image,(x,y),(x+w,y+h),(255,0,0), thickness)
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = image[y:y+h, x:x+w]
        eyes = eye_cascade.detectMultiScale(roi_gray)
        for (ex,ey,ew,eh) in eyes:
            cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),thickness)


def run_detection(image):

    print(__doc__)

    face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_alt.xml')

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = cv2.equalizeHist(gray)

    faces = face_cascade.detectMultiScale(gray, scaleFactor=1.3, minNeighbors=4, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)

    draw_detections(image, gray, faces)

    cv2.imshow('image', image)
    cv2.waitKey(3)

if __name__ == "__main__":

    rospy.init_node('ThresholdGUI', anonymous=False)
    cm = CameraManager(source=CameraManager.CameraSource.WebcamTop)
    cm.start()

    while True:

        if cm.current_image is not None:

            im = np.copy(cm.current_image)
            im = cv2.resize(im, (0, 0), fx=0.66667, fy=0.66667)

            run_detection(im)
            time.sleep(0.05)


