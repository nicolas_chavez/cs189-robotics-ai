import math
import numpy as np
import cv2
from toolbox import Orientation
from toolbox import CellStatus
from SensorManagers.depth_manager2 import DepthManager2
import time
import rospy


class ObstacleLobe:

    def __init__(self):

        self.started = False

        # what threshold value for mean to be to think there's an object
        self.object_present_mean_thresh = 0.35

    def start(self):

        self.started = True

    def scan(self, depth_image):

        """

        Discretizes the image into 2 vertical stripes, returns which stripe has objects close by

        :param depth_image:
        :return: left and right object presences
        """

        if depth_image is None:
            return None

        # split image along vertical segments
        # in our case we use 2 becuase there's a left ahead tile and a right ahead tile
        im_h, im_w = depth_image.shape
        num_segments = 3
        segment_size = float(im_w) / num_segments
        segment_size = int(math.floor(segment_size))

        # calculate mean pixel intesity in each segment
        means = np.zeros(num_segments)

        # is an object present in that segment close by
        presences = np.zeros(num_segments)

        for i in range(num_segments):

            left_edge = segment_size*i
            right_edge = segment_size*(i + 1)

            segment = depth_image[0:im_h, left_edge:right_edge]

            # square root so that closer things are more cost if we
            # dont' end up using binarized data
            mean = cv2.mean(np.sqrt(segment))
            mean = mean[0]

            means[i] = mean

            # print means

            # if the mean value of the segment has many NaNs i.e. many zeros
            # there is an object very close by
            # set presence flag to zero
            if mean < self.object_present_mean_thresh:
                presences[i] = 1

        return presences

if __name__ == "__main__":

    rospy.init_node('Obstacle', anonymous=False)
    dm = DepthManager2()
    ol = ObstacleLobe()
    ol.start()
    dm.start()

    while True:

        if dm.current_image is not None:
            n_im = np.copy(dm.current_image)
            n_im = dm.near_sighted(n_im)
            DepthManager2.debug_draw(dm.current_image)

            p = ol.scan(n_im)
            print p

        time.sleep(0.1)
