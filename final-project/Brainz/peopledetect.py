#!/usr/bin/env python

'''
example to detect upright people in images using HOG features

Usage:
    peopledetect.py <image_names>

Press any key to continue, ESC to stop.
'''

# Python 2/3 compatibility
from __future__ import print_function

import sys
from glob import glob
import itertools as it
import time

import numpy as np
import cv2

import rospy
from SensorManagers.camera_manager import CameraManager


def inside(r, q):
    rx, ry, rw, rh = r
    qx, qy, qw, qh = q
    return rx > qx and ry > qy and rx + rw < qx + qw and ry + rh < qy + qh


def draw_detections(img, rects, thickness = 1):
    for x, y, w, h in rects:
        # the HOG detector returns slightly larger rectangles than the real objects.
        # so we slightly shrink the rectangles to get a nicer output.
        pad_w, pad_h = int(0.15*w), int(0.05*h)
        cv2.rectangle(img, (x+pad_w, y+pad_h), (x+w-pad_w, y+h-pad_h), (0, 255, 0), thickness)


def run_detection(image):

    print(__doc__)

    hog = cv2.HOGDescriptor()
    hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())

    found, w = hog.detectMultiScale(image, winStride=(8, 8), padding=(32, 32), scale=1.05)
    found_filtered = []
    for ri, r in enumerate(found):
        for qi, q in enumerate(found):
            if ri != qi and inside(r, q):
                break
        else:
            found_filtered.append(r)
    draw_detections(image, found)
    draw_detections(image, found_filtered, 3)
    # print('%d (%d) found' % (len(found_filtered), len(found)))
    cv2.imshow('img', image)
    cv2.waitKey(3)

if __name__ == "__main__":

    rospy.init_node('ThresholdGUI', anonymous=False)
    cm = CameraManager(source=CameraManager.CameraSource.WebcamTop)
    cm.start()

    while True:

        if cm.current_image is not None:

            im = np.copy(cm.current_image)
            im = cv2.resize(im, (0, 0), fx=0.3, fy=0.3)

            im = cv2.transpose(im)

            run_detection(im)
            time.sleep(0.05)


