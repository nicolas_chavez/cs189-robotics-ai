import math
import numpy as np
from toolbox import Orientation


class SupernavLobe:

    def __init__(self):

        self.navigation = None
        self.started = False

    def start(self, navigation):

        self.navigation = navigation
        self.started = True

    def travel_to_target_obs(self, obstacle_blocks, target):

        current_pose = self.navigation.get_current_pose()

        print "======"
        p = self.possible_poses_for_obs(obstacle_blocks, target)
        next_local_pose = self.best_next_pose(p, target)

        print current_pose
        print obstacle_blocks
        print next_local_pose

        if next_local_pose is not None:
            self.move_local(next_local_pose)

    def possible_poses_for_obs(self, obstacle_blocks, target):

        p = []

        a = 0  # self.target_facing_local(target)
        if a is None:
            _, _, a = self.navigation.get_current_pose()

        # SUPER LEFT
        if cmp([False, True, True], obstacle_blocks) == 0:

            p = [(0.1, 0.15, a)]
            print "SUPER LEFT"

        # LEFT
        elif cmp([False, False, True], obstacle_blocks) == 0:

            # p = [(0.2, 0.3, a), (0.2, 0.15, a)]
            p = [(0.4, 0.12, a)]
            print "SLIGHT LEFT"

        # RIGHT
        elif cmp([True, False, False], obstacle_blocks) == 0:

            # p = [(0.2, -0.3, a), (0.2, -0.15, a)]
            p = [(0.4, -0.12, a)]
            print "SLIGHT RIGHT"

        # SUPER RIGHT
        elif cmp([True, True, False], obstacle_blocks) == 0:

            p = [(0.1, -0.15, a)]
            print "SUPER RIGHT"

        # THRU PASS
        elif cmp([True, False, True], obstacle_blocks) == 0:

            p = [(0.4, 0, a)]
            print "THRU PASS"

        # WIDE OPEN
        elif cmp([False, False, False], obstacle_blocks) == 0:

            p = [
                # (0.2, 0.3, a),
                # (0.2, 0.15, a),
                (0.5, 0, a),
                # (0.2, -0.15, a),
                # (0.2, -0.3, a)
            ]
            print "WIDE OPEN"

        # SIDES OPEN, BLOCK MID
        elif cmp([False, True, False], obstacle_blocks) == 0:

            p = [(0.2, 0.3, a), (0.2, -0.3, a)]
            print "SIDES OPEN"

        # ALL BLOCKED
        elif cmp([True, True, True], obstacle_blocks) == 0:

            p = [(0.0, 0.3, a), (0.0, -0.3, a)]
            print "ALL BLOCKED"

        return p

    def best_next_pose(self, next_poses, target):

        t_x, t_y, t_is_home = target
        best_next_local_pose = None
        best_val = float("inf")
        for next_local_pose in next_poses:

            next_global_pose = self.local_to_global(self.navigation.get_current_pose(), next_local_pose)
            n_x, n_y, _ = next_global_pose
            distance = ((n_x - t_x)**2 + (n_y - t_y)**2)**0.5

            if distance < best_val:
                best_val = distance
                best_next_local_pose = next_local_pose

        return best_next_local_pose

    def move_local(self, local_pose):

        current_pose = self.navigation.get_current_pose()
        next_pose = self.local_to_global(current_pose, local_pose)

        self.navigation.GOTO_turns(next_pose)

    @staticmethod
    def global_to_local(current_global_pose, global_pose):

        x, y, theta = global_pose
        c_x, c_y, c_theta = current_global_pose

        initial_vec = np.array([x, y])
        initial_vec -= np.array([c_x, c_y])
        rot_ang = -math.radians(c_theta)
        rot_mat = np.array([[np.cos(rot_ang), -np.sin(rot_ang)], [np.sin(rot_ang), np.cos(rot_ang)]])
        transformed_vec = rot_mat.dot(initial_vec)

        x = transformed_vec[0]
        y = transformed_vec[1]

        theta -= c_theta

        # correct theta to -180 to 180
        if theta <= -180:
            theta += 360
        elif theta > 180:
            theta -= 360

        return x, y, theta

    @staticmethod
    def local_to_global(current_global_pose, local_pose):

        c_x, c_y, c_theta = current_global_pose
        l_x, l_y, l_theta = local_pose

        initial_vec = np.array([l_x, l_y])
        rot_ang = math.radians(c_theta)
        rot_mat = np.array([[np.cos(rot_ang), -np.sin(rot_ang)], [np.sin(rot_ang), np.cos(rot_ang)]])
        transformed_vec = rot_mat.dot(initial_vec)
        transformed_vec += np.array([c_x, c_y])

        x = transformed_vec[0]
        y = transformed_vec[1]

        theta = l_theta + c_theta

        # correct theta to -180 to 180
        if theta <= -180:
            theta += 360
        elif theta > 180:
            theta -= 360

        return x, y, theta

    def back_up_turn(self, bump_dir):

        # Back it up, but only a tiny bit and turn away

        turn_angle = 0
        if bump_dir is Orientation.North:
            turn_angle = 180
        elif bump_dir is Orientation.West:
            turn_angle = 90
        elif bump_dir is Orientation.East:
            turn_angle = -90

        self.move_local((-0.5, 0, turn_angle))

    def target_facing_local(self, target):

        current_pose = self.navigation.get_current_pose()
        c_x, c_y, c_theta = current_pose
        global_face_angle = self.target_facing_global(target)
        if global_face_angle is not None:

            return global_face_angle - c_theta
        else:
            return None

    def target_facing_global(self, target):

        current_pose = self.navigation.get_current_pose()
        c_x, c_y, c_theta = current_pose

        # turn to
        # target = (None, None, False)  # some x, y, is_home_target
        t_x, t_y, t_is_home = target
        if t_x is not None and t_y is not None:
            return math.degrees(np.arctan2((t_y - c_y), (t_x - c_x)))
        else:
            return None

    def home_to_target(self, home):

        bb, dist, angle = home

        x = math.cos(math.radians(angle)) * dist
        y = math.sin(math.radians(angle)) * dist

        return x, y, True

    def turn_to_target(self, target, home_visible=False, home_angle=0):

        current_pose = self.navigation.get_current_pose()
        c_x, c_y, c_theta = current_pose

        # turn to
        # target = (None, None, False)  # some x, y, is_home_target
        t_x, t_y, t_is_home = target

        if not t_is_home and t_x is not None and t_y is not None:

            # turn to target
            face_theta = self.target_facing_global(target)
            self.navigation.turn_angle(face_theta)
            # self.navigation.GOTO_turns((c_x, c_y, face_theta))
            return True

        elif t_is_home and home_visible:

            # turn to target
            face_theta = c_theta + home_angle
            self.navigation.turn_angle(face_theta)
            # self.navigation.GOTO_turns((c_x, c_y, face_theta))
            return True

        elif t_is_home and t_x is not None and t_y is not None:

            # turn to target
            face_theta = self.target_facing_global(target)
            self.navigation.turn_angle(face_theta)
            # self.navigation.GOTO_turns((c_x, c_y, face_theta))
            return True

        else:

            # freak out, target is garb
            return False

if __name__ == "__main__":

    import rospy
    from Brainz.navigation_lobe2 import NavigationLobe2
    from SensorManagers.odom_manager import OdomManager
    from OutputManagers.motion_manager import MotionManager

    rospy.init_node('SupernavTest', anonymous=False)
    mot = MotionManager()
    odo = OdomManager()
    nav = NavigationLobe2()
    snl = SupernavLobe()

    mot.start()
    odo.start()
    nav.start(mot, odo)
    snl.start(nav)

    snl.move_local((3, 0, 0))
    snl.move_local((-3, 0, 0))
