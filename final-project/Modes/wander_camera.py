
import rospy
import numpy as np
import cv2

from threading import Thread

from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

from Brainz.ObjectDetectors.home_detector import HomeDetector
from enum import Enum

# Handles cleaning up camera images and controlling vision modules


class WanderCamera:

    CameraSource = Enum('CameraSource', 'Asus WebcamTop WebcamBottom')
    ObjectTarget = Enum('ObjectTarget', 'Home Face')

    def __init__(self, source=CameraSource.Asus):

        # Reference image for vision back projection modules

        if source is WanderCamera.CameraSource.Asus:
            self.rostopic = '/camera/rgb/image_raw'
        elif source is WanderCamera.CameraSource.WebcamTop:
            self.rostopic = '/webcam/top'
        elif source is WanderCamera.CameraSource.WebcamBottom:
            self.rostopic = '/webcam/bottom'

        self.home_detector = HomeDetector()
        self.face_detector = HomeDetector()

        self.debug_image = None

        self.started = False

        # callback tracker across threads
        self.detect_tracker = {
            WanderCamera.ObjectTarget.Home: None,
            WanderCamera.ObjectTarget.Face: None
        }

        self.cv_bridge = CvBridge()

        # If still interested in detecting certain objects
        self.to_detect = {
            WanderCamera.ObjectTarget.Home: True,
            WanderCamera.ObjectTarget.Face: True
        }

    def start(self):

        self.started = True
        rospy.Subscriber(self.rostopic, Image, self.on_camera_image, queue_size=1,  buff_size=2**24)

    # Does the heavy lifting
    def process_image(self, image):

        home_rect = None
        face_rect = None

        # If still interested in detecting, then try to do so
        if self.to_detect[WanderCamera.ObjectTarget.Home]:
            home_rect, home_dist, home_ang = self.home_detector.detect(image)

        if self.to_detect[WanderCamera.ObjectTarget.Face]:
            face_rect, face_dist, face_ang = None, None, None

        # Save these results to be called on main thread

        if home_rect is not None:
            self.store_detect_data(home_rect, home_dist, WanderCamera.ObjectTarget.Home)
        else:
            self.detect_tracker[WanderCamera.ObjectTarget.Home] = None

        if face_rect is not None:
            self.store_detect_data(face_rect, face_dist, WanderCamera.ObjectTarget.Face)
        else:
            self.detect_tracker[WanderCamera.ObjectTarget.Face] = None

    # Save detection data
    def store_detect_data(self, bounding_rect, est_distance, obj_kind):

        self.detect_tracker[obj_kind] = (bounding_rect, est_distance, obj_kind)

    # Debug the dectections by drawing them
    def draw_detections(self, image):

        colors = [(0, 0, 255), (255, 0, 0)]

        # HOME RED
        # FACE BLUE

        obj_kinds = self.detect_tracker.keys()

        for i in range(len(obj_kinds)):

            obj_kind = obj_kinds[i]

            track_data = self.detect_tracker[obj_kind]

            if track_data is not None:
                r, _, _ = track_data
                x, y, w, h = r

                cv2.rectangle(image, (x, y), (x+w, y+h), colors[i], thickness=3)

    # Called when image from CV is received
    def on_camera_image(self, data):

        if not self.started:
            return

        image = self.cv_bridge.imgmsg_to_cv2(data, 'bgr8')

        # Crop the same way depth images are cropped
        cropping_rect = (50, 120, 540, 340)
        crop_x, crop_y, crop_w, crop_h = cropping_rect
        image = image[crop_y:crop_y+crop_h, crop_x:crop_x+crop_w]

        if image is not None:

            display_image = np.copy(image)

            self.process_image(image)

            self.draw_detections(display_image)

            self.debug_image = display_image

