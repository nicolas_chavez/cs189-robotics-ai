
import time
import math
import cv2
import numpy as np
import rospy

from sensor_msgs.msg import PointCloud2, LaserScan, Image
from cv_bridge import CvBridge, CvBridgeError

# Handles receiving and cleaning up depth data


class WanderDepth:

    def __init__(self):

        # tracks the last millisecond reading (for calculating FPS)
        self.last_millis = 0

        self.started = False

        self.bridge = CvBridge()

        self.current_image = None

    def start(self):

        self.started = True
        rospy.Subscriber('/camera/depth/image', Image, self.on_depth_image, queue_size=1, buff_size=2 ** 24)

    def on_depth_image(self, data):

        self.current_image = data
        self.process(data)

    def process(self, data):

        if data is None:
            return None
        # Use bridge to convert to CV::Mat type.
        # NOTE: SOME DATA WILL BE 'NaN'
        # and numbers correspond to distance to camera in meters
        cv_image = self.bridge.imgmsg_to_cv2(data, 'passthrough')

        # manipulate the image directly here!
        # Crop the image to remove irrelevant info (bars in front of robot)
        # cropping_rect = (50, 120, 540, 340)
        cropping_rect = (80, 120, 560, 340)
        crop_x, crop_y, crop_w, crop_h = cropping_rect

        cv_image_cropped = cv_image[crop_y:crop_y+crop_h, crop_x:crop_x+crop_w]

        # Blur
        cv_image_cropped = cv2.GaussianBlur(cv_image_cropped, (5, 5), 1)

        cv_image_cropped = cv2.cvtColor(cv_image_cropped, cv2.COLOR_GRAY2BGR)

        # Replace Nans with 0.5, an intermediate value
        cv_image_cropped[np.isnan(cv_image_cropped)] = 0.5

        # Masks out farther stuff, only close things important
        lower_bound = np.array([0.0, 0.0, 0.0])
        upper_bound = np.array([1.2, 1.2, 1.2])

        close_mask = cv2.inRange(cv_image_cropped, lower_bound, upper_bound)
        masked = cv2.bitwise_and(cv_image_cropped, cv_image_cropped, mask=close_mask)

        # Call listener

        return masked



