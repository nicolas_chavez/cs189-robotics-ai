import rospy
import time
import cv2
import numpy as np
from enum import Enum

from toolbox import Orientation

from wander_motion import WanderMotion
from wander_camera import WanderCamera
from wander_depth import WanderDepth
from wander_navigation import WanderNavigation

from OutputManagers.speech_manager import Sayings

from Brainz.ObjectDetectors.FaceDetector.face_detector import FaceDetector
from Brainz.ObjectDetectors.tray_detector import TrayDetector
from SensorManagers.camera_manager import CameraManager

from OutputManagers.speech_manager import SpeechManager
from OutputManagers.motion_manager import MotionManager
from SensorManagers.depth_manager2 import DepthManager2
from SensorManagers.odom_manager import OdomManager
from Brainz.obstacle_lobe import ObstacleLobe
from Brainz.navigation_lobe2 import NavigationLobe2
from Brainz.supernav_lobe import SupernavLobe

from SensorManagers.bump_manager import BumpManager
from SensorManagers.cliff_manager import CliffManager
from SensorManagers.wheel_drop_manager import WheelDropManager
from SensorManagers.key_manager import KeyManager
from OutputManagers.sound_manager import SoundManager

ButlerState = Enum('ButlerState', 'Refill Wander HomeOdom HomeVision Scan AimFace WaitFood')


class WanderMode:

    # Time to wait since last callbacks to return to default states
    BOX_OUT_TIMEOUT_MS = 1500
    OBJ_DETECT_TIMEOUT_MS = 500
    WANDER_TIMEOUT_MS = 10000
    WAIT_FOOD_TIMEOUT_MS = 5000
    AIM_FACE_TIMEOUT_MS = 1000
    SCAN_TIMEOUT_MS = 5000

    START_FOOD_COUNT = 8

    def __init__(self):

        self.is_shutdown = False

        # Boxed out means the robot is trapped by the obstacles in front of it
        self.is_boxed_out = False

        # The direction with the most obstacles
        self.box_out_dir = None
        # The last time since it was boxed out, keep track of to return to normal state if not continued
        self.last_box_out_time = 0

        # The last time an object was detected, keep track of to return to non homing state
        self.last_obj_detect_time = 0

        self.motion = WanderMotion()
        self.key = KeyManager()
        self.sound = SoundManager()
        self.asus_camera = WanderCamera(source=WanderCamera.CameraSource.Asus)
        self.bump = BumpManager()
        self.cliff = CliffManager()
        self.depth = WanderDepth()
        self.wheel_drop = WheelDropManager()

        self.navigation = WanderNavigation()

        self.top_camera = CameraManager(source=CameraManager.CameraSource.WebcamTop)
        self.bottom_camera = CameraManager(source=CameraManager.CameraSource.WebcamBottom)
        self.face_detector = FaceDetector()
        self.tray_detector = TrayDetector()

        self.speech = SpeechManager()
        self.motion2 = MotionManager()
        self.depth2 = DepthManager2()
        self.obstacle = ObstacleLobe()
        self.odom = OdomManager()
        self.nav2 = NavigationLobe2()
        self.supernav = SupernavLobe()

        # The current target's bounding box, its estimated distance, and the kind of object it is
        self.current_target = None, None, None

        # If an object was just claimed in last cycle

        self.home_pose = (0, 0, 0)

        self.butler_state = ButlerState.HomeVision

        self.face_focus = None

        self.wander_start_time = 0
        self.wait_food_start_time = 0
        self.aim_face_start_time = 0
        self.scan_face_time = 0
        self.start_subscan_time = 0

        self.food_count = WanderMode.START_FOOD_COUNT

    def start(self):

        # Initialization

        # Start modules

        self.motion.start(), self.key.start(), self.sound.start(), self.asus_camera.start(), self.bump.start()
        self.cliff.start(), self.depth.start(), self.wheel_drop.start(), self.navigation.start()

        # self.depth2.start()
        self.motion2.start(), self.obstacle.start(), self.odom.start(), self.speech.start()
        self.nav2.start(self.motion2, self.odom), self.supernav.start(self.nav2)

        self.top_camera.start()
        self.bottom_camera.start()

        # rotate away if cliffed
        self.cliff.on_cliff_callback = self.on_cliff
        # shutdown if wheel dropped
        self.wheel_drop.on_drop_callback = self.shutdown
        # callback for boxed out
        self.navigation.boxed_out_callback = self.on_box_out

        self.speech.say(Sayings.RefillState)
        self.speech.say(Sayings.RefillMePlease)

    def update(self):

        self.process_home_detection()
        self.process_depth_image()

        self.handle_keys()

        if self.butler_state is ButlerState.WaitFood:

            pass  # do nothing, timing out in update_timeouts

        if self.butler_state is ButlerState.Refill:

            self.motion.stop_moving()
            self.sound.make_beep()

            # save the odom position
            self.home_pose = self.nav2.get_current_pose()

        elif self.butler_state is ButlerState.Scan:

            # scan 360 degrees
            self.scan_for_faces()

        # If there is a bump, back up and turn in the appropriate direction
        elif self.bump.have_bumped():
            # Back it up, but only a tiny bit
            bump_dir = self.bump.bump_dir
            self.motion.move_backward()
            self.motion.rotate_away(bump_dir)

        elif self.butler_state is ButlerState.HomeOdom:

            self.move_home()

        elif self.is_boxed_out:
            # We're trapped, so rotate until not case anymore
            self.motion.rotate_tiny_bit(self.box_out_dir)

        elif self.butler_state is ButlerState.AimFace:

            self.face_focus = self.detect_face()

            if self.face_focus is None:

                pass  # timing out taken care of below
            else:

                self.aim_face_start_time = self.current_millis()

                if abs(self.face_focus) < 0.1:

                    self.speech.say(Sayings.HelloRobot)
                    self.speech.say(Sayings.WaitFoodState)

                    self.butler_state = ButlerState.WaitFood
                    self.wait_food_start_time = self.current_millis()

                else:
                    ang_vel = -0.09 * self.face_focus
                    self.motion.stop_moving()
                    self.motion.move_forward(lin_vel=0, ang_vel=ang_vel)

        elif self.butler_state is ButlerState.Wander or \
                self.butler_state is ButlerState.HomeVision:

            # Move forward otherwise
            best_horizontal = self.navigation.route_decision()
            ang_vel = -0.45 * best_horizontal
            self.motion.move_forward(lin_vel=0.2, ang_vel=ang_vel)

        self.update_timeouts()

        self.debug_draws()

        return self.is_shutdown

    def debug_draws(self):

        if self.asus_camera.debug_image is not None and self.navigation.debug_image is not None:

            cv2.imshow('Colour', self.asus_camera.debug_image)
            cv2.imshow('Depf', self.navigation.debug_image)

        if self.face_detector.debug_image is not None:
            cv2.imshow('Faces', self.face_detector.debug_image)

        cv2.waitKey(3)

    def detect_face(self):

        if self.top_camera.current_image is not None:

            img = np.copy(self.top_camera.current_image)
            img = cv2.resize(img, (0, 0), fx=0.66667, fy=0.66667)
            im_h, im_w, _ = img.shape
            rects = self.face_detector.detect(img)

            wts = []
            tot_weight = 0

            if len(rects) > 0:

                for x, y, w, h in rects:

                    weight = w*h
                    center = float(x + (w/2))
                    center /= float(im_w)
                    center *= 2.0
                    center -= 1.0

                    wts.append((weight, center))
                    tot_weight += weight

                horizontal_center = 0

                for wt, ctr in wts:

                    horizontal_center = ctr*(float(wt)/tot_weight)

                return horizontal_center  # horizontal_center  # number from -1 to 1 left to right

        return None

    def update_timeouts(self):

        r = rospy.Rate(10)

        # Keep track of time since being boxed out and homing
        cur_millis = self.current_millis()
        # Not boxed out anymore
        if cur_millis - self.last_box_out_time > WanderMode.BOX_OUT_TIMEOUT_MS:
            self.is_boxed_out = False
            self.box_out_dir = None

        # Not homing on target anymore
        if cur_millis - self.last_obj_detect_time > WanderMode.OBJ_DETECT_TIMEOUT_MS:
            self.current_target = None, None, None

        # Stop wandering, do scan mode
        if cur_millis - self.wander_start_time > WanderMode.WANDER_TIMEOUT_MS and \
                self.butler_state is ButlerState.Wander:

            self.speech.say(Sayings.ScanState)
            self.speech.say(Sayings.WhereIsEverybody)

            self.butler_state = ButlerState.Scan
            self.scan_face_time = 0
            self.start_subscan_time = cur_millis

        # Stop wandering, do scan mode
        if cur_millis - self.aim_face_start_time > WanderMode.AIM_FACE_TIMEOUT_MS and \
                self.butler_state is ButlerState.AimFace:

            self.speech.say(Sayings.ScanState)

            self.butler_state = ButlerState.Scan
            self.start_subscan_time = cur_millis

        if self.scan_face_time + (cur_millis - self.start_subscan_time) > WanderMode.SCAN_TIMEOUT_MS and \
                self.butler_state is ButlerState.Scan:

            # scan done, wander the earth like Moses
            self.speech.say(Sayings.WanderState)
            self.butler_state = ButlerState.Wander
            self.wander_start_time = cur_millis

        if cur_millis - self.wait_food_start_time > WanderMode.WAIT_FOOD_TIMEOUT_MS and \
                self.butler_state is ButlerState.WaitFood:

            cur_food_count = self.read_food_count()
            if cur_food_count < self.food_count:

                self.food_taken()
                self.food_count = cur_food_count

                if self.food_count <= 0:  # Go refill!
                    self.speech.say(Sayings.HomeOdomState)
                    self.butler_state = ButlerState.HomeOdom
                else:
                    self.speech.say(Sayings.WanderState)
                    self.butler_state = ButlerState.Wander
                    self.wander_start_time = cur_millis

            else:

                self.speech.say(Sayings.NiceToMeet)
                self.speech.say(Sayings.WanderState)
                self.butler_state = ButlerState.Wander
                self.wander_start_time = cur_millis

        # wait for 0.1 seconds (10 HZ) and publish again
        r.sleep()

    def scan_for_faces(self):

        # move around in 360 deg, if nothing, then Wander again, update self.face_focus

        self.motion.move_forward(lin_vel=0, ang_vel=-0.07)

        face_horiz = self.detect_face()

        self.face_focus = face_horiz

        if self.face_focus is not None:

            self.speech.say(Sayings.Ooh)
            self.speech.say(Sayings.AimFaceState)
            self.butler_state = ButlerState.AimFace
            self.scan_face_time += (self.current_millis() - self.start_subscan_time)
            self.aim_face_start_time = self.current_millis()

    def food_taken(self):

        self.speech.say(Sayings.MyPleasure)

        if self.bottom_camera.current_image is not None:

            food_im = np.copy(self.bottom_camera.current_image)
            food_im = cv2.resize(food_im, (0, 0), fx=0.3, fy=0.3)
            food_im = food_im[:, 180:423]
            pass
            # TODO: Tweet this picture...

    def handle_keys(self):

        if self.key.poll() == "1\n":

            # Quit home mode
            self.speech.say(Sayings.WanderState)
            self.speech.say(Sayings.HeadingOutNow)

            self.butler_state = ButlerState.Wander
            self.food_count = self.read_food_count()
            self.wander_start_time = self.current_millis()
            self.motion.rotate_away(Orientation.North)

        elif self.key.poll() == "2\n":

            # Quit home mode
            self.speech.say(Sayings.HomeOdomState)
            self.butler_state = ButlerState.HomeOdom

    def read_food_count(self):

        readings = []
        for _ in range(4):

            if self.bottom_camera.current_image is not None:

                im = np.copy(self.bottom_camera.current_image)
                im = cv2.resize(im, (0, 0), fx=0.3, fy=0.3)
                im = im[:, 180:423]
                count = self.tray_detector.count_blobs(im)
                readings.append(count)

        num_circles = 0
        if len(readings) > 0:
            num_circles = max(set(readings), key=readings.count)

        num_food = WanderMode.START_FOOD_COUNT - num_circles

        print "Food: " + str(num_food)

        return num_food

    def move_home(self):

        cur_x, cur_y, _ = self.nav2.get_current_pose()
        home_x, home_y, _ = self.home_pose
        home_target = (home_x, home_y, True)

        _ = self.supernav.turn_to_target(home_target)

        dist_to_home = ((cur_x - home_x)**2 + (cur_y - home_y)**2)**0.5

        if dist_to_home < 1.5:
            # Scavenge it!
            self.speech.say(Sayings.HomeVisionState)
            self.butler_state = ButlerState.HomeVision
            return

        depth2_im = self.depth2.process(self.depth.current_image)
        if depth2_im is not None:
            n_im = np.copy(depth2_im)
            n_im = self.depth2.near_sighted(n_im)
            DepthManager2.debug_draw(depth2_im)

            presences = self.obstacle.scan(n_im)

            obstacle_blocks = []
            for v in presences:
                t = True
                if v == 1.0:
                    t = False
                obstacle_blocks.append(t)

            self.supernav.travel_to_target_obs(obstacle_blocks, home_target)

    def process_home_detection(self):

        for obj_kind in [WanderCamera.ObjectTarget.Home, WanderCamera.ObjectTarget.Face]:
            if self.asus_camera.detect_tracker[obj_kind] is not None:
                # Update tracking data, but only if not claimed yet
                self.current_target = self.asus_camera.detect_tracker[obj_kind]
                self.last_obj_detect_time = int(round(time.time() * 1000))

    def process_depth_image(self):

        depth_im = self.depth.process(self.depth.current_image)
        if depth_im is not None:
            target_bounding_box, target_distance, target_kind = self.current_target
            # Feed depth info to navigation
            if self.butler_state is not ButlerState.HomeVision:
                _, _, found_obj = self.navigation.route_decide(depth_im)
            else:
                _, _, found_obj = self.navigation.route_decide(depth_im, target_bounding_box, target_distance, target_kind)
            # If an object was found, claim it
            if found_obj:
                if self.butler_state is not ButlerState.Refill:
                    self.speech.say(Sayings.RefillState)
                    self.speech.say(Sayings.RefillMePlease)
                self.butler_state = ButlerState.Refill

    # Take action when faced with too many obstacles
    def on_box_out(self, boxed_dir):

        # Set the trigger and the timeout
        self.is_boxed_out = True

        cur_millis = int(round(time.time() * 1000))
        if cur_millis - self.last_box_out_time > WanderMode.BOX_OUT_TIMEOUT_MS:

            self.box_out_dir = boxed_dir

        self.last_box_out_time = cur_millis

    # Take action when cliff sensor goes off
    def on_cliff(self):

        # Back up and turn away
        self.motion.move_backward()
        self.motion.rotate_slow()

    @staticmethod
    def current_millis():
        return int(round(time.time() * 1000))

    # Stop all activities
    def shutdown(self):

        # stop turtlebot
        self.is_shutdown = True
        rospy.loginfo("Stop TurtleBot")
        self.motion.stop_moving()
        # sleep just makes sure TurtleBot receives the stop command prior to shutting down the script
        rospy.sleep(5)

    def finish(self):

        self.key.finish()

if __name__ == "__main__":

    rospy.init_node('WanderMode', anonymous=False)

    wm = WanderMode()
    wm.start()

    while True:
        if wm.update():
            break

    wm.finish()

