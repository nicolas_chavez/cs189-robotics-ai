
import rospy

from geometry_msgs.msg import Twist

import numpy as np
from math import radians
from random import randint
import time


class MotionManager:

    """

    Module in charge of applying motion. Has useful methods to just "do" stuff, like go forward and turn,
    turn slowly, back up, etc.

    Makes use of velocity smoother to move better.

    """

    def __init__(self):

        # The cmd to use to publish
        # self.cmd_vel = rospy.Publisher('cmd_vel_mux/input/navi', Twist, queue_size=10)
        self.cmd_vel = rospy.Publisher('velocity_smoother/raw_cmd_vel', Twist, queue_size=10)

        self.lin_speed = 0.5  # m/s
        self.started = False

    def start(self):

        self.started = True

    # tiny nudge to get wheels nice and loose
    def clear_nudge(self):

        r = rospy.Rate(10)

        # LINEAR CLEAR

        b = Twist()
        b.linear.x = 0.1
        b.angular.z = 0.0
        for _ in range(10):
            self.cmd_vel.publish(b)
            r.sleep()

        time.sleep(0.1)

        b.linear.x = -0.1
        b.angular.z = 0.0
        for _ in range(10):
            self.cmd_vel.publish(b)
            r.sleep()

        time.sleep(0.1)

    # move with a specified linear or angular velocity
    def move_rate(self, movement_feedback):

        r = rospy.Rate(10)

        move_cmd = Twist()
        lin, ang, halt = movement_feedback()
        move_cmd.linear.x = lin
        move_cmd.angular.z = ang

        while True:

            lin, ang, halt = movement_feedback()
            move_cmd.linear.x = lin
            move_cmd.angular.z = ang

            # if halt condition is true, break to the next motion step
            if halt:
                break

            self.cmd_vel.publish(move_cmd)

            r.sleep()

    # Stop motions
    def stop_moving(self):

        if self.started:
            # a default Twist has linear.x of 0 and angular.z of 0.  So it'll stop TurtleBot
            self.cmd_vel.publish(Twist())
