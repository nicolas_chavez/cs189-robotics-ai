
import rospy
from kobuki_msgs.msg import Sound


class SoundManager:

    """

    Handles sound outputs and has utility functions to do so.

    """

    def __init__(self):

        self.cmd_sound = rospy.Publisher('/mobile_base/commands/sound', Sound, queue_size=100)

    def start(self):

        pass

    # Make a single tone sound
    def make_beep(self):

        # shell command:
        # $ rostopic pub /mobile_base/commands/sound kobuki_msgs/Sound "value: 6"
        self.cmd_sound.publish(Sound.ON)  # "value: 6"


