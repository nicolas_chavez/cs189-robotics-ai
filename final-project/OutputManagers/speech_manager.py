import pyglet
from enum import IntEnum


class Sayings(IntEnum):

    RefillState = 0
    WanderState = 1
    HomeOdomState = 2
    HomeVisionState = 3
    ScanState = 4
    AimFaceState = 5
    WaitFoodState = 6

    RefillMePlease = 7
    HeadingOutNow = 8
    WhereIsEverybody = 9
    Ooh = 10
    HelloRobot = 11
    NiceToMeet = 12
    MyPleasure = 13


class SpeechManager:

    def __init__(self):

        self.is_started = False

        self.sayings_map = {
            Sayings.RefillState: "RefillState.wav",
            Sayings.WanderState: "WanderState.wav",
            Sayings.HomeOdomState: "HomeOdomState.wav",
            Sayings.HomeVisionState: "HomeVisionState.wav",
            Sayings.ScanState: "ScanState.wav",
            Sayings.AimFaceState: "AimFaceState.wav",
            Sayings.WaitFoodState: "WaitFoodState.wav",

            Sayings.RefillMePlease: "RefillMePlease.wav",
            Sayings.HeadingOutNow: "HeadingOutNow.wav",
            Sayings.WhereIsEverybody: "WhereIsEverybody.wav",
            Sayings.Ooh: "Ooh.wav",
            Sayings.HelloRobot: "HelloIAmARobot.wav",
            Sayings.NiceToMeet: "NiceToMeetYou.wav",
            Sayings.MyPleasure: "MyPleasure.wav",
        }

    def audio_path(self, saying):

        root = '/home/turtlebot/Desktop/Triton/FinalProject/Sounds/'
        path = root + self.sayings_map[saying]
        return path

    def start(self):

        self.is_started = True

    @staticmethod
    def exit_callback(_):

        pyglet.app.exit()

    def say(self, saying):

        if not self.is_started:
            return

        path = self.audio_path(saying)
        sound = pyglet.media.load(path, streaming=False)
        sound.play()
        d = sound.duration
        pyglet.clock.schedule_once(self.exit_callback, d)
        pyglet.app.run()

if __name__ == "__main__":

    sm = SpeechManager()
    sm.start()

    sm.say(Sayings.HomeOdomState)
