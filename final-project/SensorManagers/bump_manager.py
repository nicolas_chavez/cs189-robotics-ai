
import rospy

from kobuki_msgs.msg import BumperEvent
from toolbox import Orientation


class BumpManager:

    """

    Handles incoming bump sensor data, callbacks. Received by main bandtracker instance.

    """

    def __init__(self):

        self.bump_dir = None

    def start(self):

        rospy.Subscriber('mobile_base/events/bumper', BumperEvent, self.on_bump)

    # Process bump events
    def on_bump(self, data):

        if data.state == BumperEvent.PRESSED:

            if data.bumper - 1 == -1:

                self.bump_dir = Orientation.West
            elif data.bumper - 1 == 0:

                self.bump_dir = Orientation.North
            elif data.bumper - 1 == 1:

                self.bump_dir = Orientation.East
            else:

                self.bump_dir = None

        else:
            self.bump_dir = None

    # True if have bumped
    def have_bumped(self):

        return self.bump_dir is not None
