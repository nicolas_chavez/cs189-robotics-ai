import rospy
from cv_bridge import CvBridge
from sensor_msgs.msg import Image
from enum import Enum


class CameraManager:

    CameraSource = Enum('CameraSource', 'Asus WebcamTop WebcamBottom')

    """

    Handles cleaning up camera images, passing them back to bandtracker for recognition,
    later analysis.

    """

    def __init__(self, source=CameraSource.Asus):

        self.current_image = None

        self.started = False

        self.on_image_callback = None

        self.cv_bridge = CvBridge()

        if source is CameraManager.CameraSource.Asus:
            self.rostopic = '/camera/rgb/image_raw'
        elif source is CameraManager.CameraSource.WebcamTop:
            self.rostopic = '/webcam/top'
        elif source is CameraManager.CameraSource.WebcamBottom:
            self.rostopic = '/webcam/bottom'

    def start(self):

        self.started = True
        rospy.Subscriber(self.rostopic, Image, self.on_camera_image, queue_size=1,  buff_size=2**24)

    # Called when image from CV is received
    def on_camera_image(self, data):

        if not self.started:
            return

        image = self.cv_bridge.imgmsg_to_cv2(data, 'bgr8')

        # Crop the same way depth images are cropped
        # cropping_rect = (50, 120, 540, 340)
        # crop_x, crop_y, crop_w, crop_h = cropping_rect
        # image = image[crop_y:crop_y+crop_h, crop_x:crop_x+crop_w]

        self.current_image = image

        if self.on_image_callback:
            self.on_image_callback(self.current_image)


if __name__ == "__main__":

    import time
    import cv2
    import numpy as np
    rospy.init_node('CameraTest', anonymous=False)
    cm = CameraManager(source=CameraManager.CameraSource.WebcamTop)
    cm.start()

    while True:

        if cm.current_image is not None:
            resized = np.copy(cm.current_image)
            resized = cv2.resize(resized, (0, 0), fx=0.3, fy=0.3)
            cv2.imshow('CameraTest', resized)
            cv2.waitKey(3)

        time.sleep(0.1)
