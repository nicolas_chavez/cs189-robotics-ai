
import time
import math
import cv2
import numpy as np
import rospy

from sensor_msgs.msg import PointCloud2, LaserScan, Image
from cv_bridge import CvBridge, CvBridgeError


class DepthManager:

    """

    Handles receiving and cleaning up depth data to pass onto vision lobe eventually.
    Doesn't do too much except crop and abstract ros subscriptions.

    """

    def __init__(self):

        self.current_image = None

        self.started = False

        # callback for depth info processed
        self.on_image_callback = None  # (depth_image)

        self.bridge = CvBridge()

    def start(self):

        self.started = True
        rospy.Subscriber('/camera/depth/image', Image, self.on_depth_image, queue_size=1, buff_size=2 ** 24)

    def on_depth_image(self, data):

        if not self.started:
            return

        # Use bridge to convert to CV::Mat type.
        # NOTE: SOME DATA WILL BE 'NaN'
        # and numbers correspond to distance to camera in meters
        cv_image = self.bridge.imgmsg_to_cv2(data, 'passthrough')

        # Crop the image to remove irrelevant info (bars in front of robot)
        cropping_rect = (0,0,639,479) #(50, 120, 540, 340)
        crop_x, crop_y, crop_w, crop_h = cropping_rect

        cv_image_cropped = cv_image[crop_y:crop_y+crop_h, crop_x:crop_x+crop_w]

        # Blur
        cv_image_cropped = cv2.GaussianBlur(cv_image_cropped, (5, 5), 1)

        cv_image_cropped = cv2.cvtColor(cv_image_cropped, cv2.COLOR_GRAY2BGR)

        self.current_image = cv_image_cropped

        # Call listener
        if self.on_image_callback is not None:
            self.on_image_callback(self.current_image)

    def binarize_depth_image(self, depth_image):

        """

        Makes everything that is NaN (i.e. < 20 cm) zero and everything
        that is non NaN (i.e. > 20 cm) one

        :param depth_image: Raw depth image
        :return: Binarized depth image
        """

        if depth_image is None:
            return depth_image

        # if depth image value is NaN set equal to 0, if not NaN set equal to 1
        depth_image = np.logical_not(np.isnan(depth_image)).astype(int)

        return depth_image


    def draw_image_and_obstacles(self, image, draw_rect=None, means=None):

        """

        Draws the obstacles on depth image, for DEBUG purposes

        :param image: Image to draw on
        :param draw_rect: Where to draw on image
        :param means: The means on each strip from depth_image_means
        :return: Nothing, just shows image
        """

        seg_height = 40

        if draw_rect is None:

            d_x = 0
            d_y = 0
            d_h, d_w, _ = image.shape

        else:
            d_x, d_y, d_w, d_h = draw_rect

        # draw rectangle corresponding to means size at the top of the depth image
        if means is not None:

            num_segments = np.size(means)

            segment_size = float(d_w) / num_segments
            segment_size = int(math.floor(segment_size))

            means -= np.amin(means)
            if np.amax(means) != 0:
                means /= np.amax(means)


            for i in range(num_segments):

                left_edge = segment_size*i

                r = int(255.0 * means[i])
                b = 255 - r

                cv2.rectangle(image, (d_x + left_edge, d_y), (d_x + left_edge + segment_size, d_y + int(seg_height * (1-means[i]))), (b, 0, r), thickness=-1)

        # display the depth image
        cv2.imshow('Actual', image)
        cv2.waitKey(3)
