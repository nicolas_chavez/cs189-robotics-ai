
import time
import math
import cv2
import numpy as np
import rospy

from sensor_msgs.msg import PointCloud2, LaserScan, Image
from cv_bridge import CvBridge, CvBridgeError


class DepthManager2:

    """

    Handles receiving and cleaning up depth data to pass onto vision lobe eventually.
    Doesn't do too much except crop and abstract ros subscriptions.

    """

    def __init__(self):

        self.current_image = None

        self.lower_bound = np.array([0.0])
        self.upper_bound = np.array([1.0])

        self.started = False

        # callback for depth info processed
        self.on_image_callback = None  # (depth_image)

        self.bridge = CvBridge()

    def start(self):

        self.started = True
        rospy.Subscriber('/camera/depth/image', Image, self.on_depth_image, queue_size=1, buff_size=2 ** 24)

    def on_depth_image(self, data):

        self.current_image = data
        self.process(data)

    def process(self, data):

        if data is None:
            return None
        # Use bridge to convert to CV::Mat type.
        # NOTE: SOME DATA WILL BE 'NaN'
        # and numbers correspond to distance to camera in meters
        cv_image = self.bridge.imgmsg_to_cv2(data, 'passthrough')

        """
        79, 14, 530, 450
        # Crop the image to remove irrelevant info (bars in front of robot)
        cropping_rect = (0,0,639,479) #(50, 120, 540, 340)
        crop_x, crop_y, crop_w, crop_h = cropping_rect

        cv_image_cropped = cv_image[crop_y:crop_y+crop_h, crop_x:crop_x+crop_w]
        """

        cropping_rect = (79, 14, 530, 450)
        crop_x, crop_y, crop_w, crop_h = cropping_rect
        cv_image_cropped = cv_image[crop_y:crop_y+crop_h, crop_x:crop_x+crop_w]

        # cv_image_cropped = cv_image

        # Blur
        cv_image_cropped = cv2.GaussianBlur(cv_image_cropped, (5, 5), 1)

        return cv_image_cropped

    def near_sighted(self, image):

        # Masks out items farther than a certain distance
        if image is not None:
            close_mask = cv2.inRange(image, self.lower_bound, self.upper_bound)
            masked = cv2.bitwise_and(image, image, mask=close_mask)
            return masked

        return None

    @staticmethod
    def is_valid_image(image):

        if image is not None:

            if image.size > 0:
                return True

        return False

    @staticmethod
    def debug_draw(image):

        if DepthManager2.is_valid_image(image):

            image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)

            im_h, im_w, im_d = image.shape

            image /= float(10)
            image = np.ones(image.shape, dtype=np.float32) - image

            nan_indices = np.isnan(image)
            nan_indices = nan_indices[:, :, 0]
            nan_b = np.dstack((nan_indices, np.zeros((im_h, im_w), dtype=bool), np.zeros((im_h, im_w), dtype=bool)))
            nan_g = np.dstack((np.zeros((im_h, im_w), dtype=bool), nan_indices, np.zeros((im_h, im_w), dtype=bool)))
            nan_r = np.dstack((np.zeros((im_h, im_w), dtype=bool), np.zeros((im_h, im_w), dtype=bool), nan_indices))

            image[nan_b] = 0.0
            image[nan_g] = 0.0
            image[nan_r] = 1.0

            # draw 2 lines
            cv2.line(image, (im_w/3, 0), (im_w/3, im_h), color=(255, 0, 0), thickness=1)
            cv2.line(image, (2*im_w/3, 0), (2*im_w/3, im_h), color=(255, 0, 0), thickness=1)

            # display the depth image
            cv2.imshow('Actual', image)
            cv2.waitKey(3)


if __name__ == "__main__":

    rospy.init_node('DepthManager', anonymous=False)
    dm = DepthManager2()
    dm.start()

    while True:

        di = dm.process(dm.current_image)
        DepthManager2.debug_draw(di)
        time.sleep(0.1)
