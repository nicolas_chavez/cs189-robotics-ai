import pyxhook


class KeyManager:

    def __init__(self):

        self.is_started = False

        # Create hookmanager
        self.hookman = pyxhook.HookManager()

        self.buffer = []

    def start(self):

        self.is_started = True

        # Define our callback to fire when a key is pressed down
        self.hookman.KeyDown = self.kbevent
        # Hook the keyboard
        self.hookman.HookKeyboard()
        # Start our listener
        self.hookman.start()

    def finish(self):

        self.hookman.cancel()

    # This function is called every time a key is presssed

    def kbevent(self, event):

        c = ''
        if 0 < event.Ascii < 128:
            c = chr(event.Ascii)

        elif event.Key == 'Return' or event.Key == 'P_Enter':
            c = '\n'

        elif event.Key == 'P_End':
            c = '1'
        elif event.Key == 'P_Down':
            c = '2'
        elif event.Key == 'P_Begin':
            c = '3'

        if len(c) > 0:

            self.buffer.append(c)
            if len(self.buffer) > 10:
                self.buffer = self.buffer[1:]

    def poll(self, length=2):

        if length > len(self.buffer):
            return ""
        else:
            buff = self.buffer[-length:]
            self.buffer = self.buffer[:-length];
            return ''.join(buff)

if __name__ == "__main__":

    import time

    km = KeyManager()
    km.start()

    while True:

        if km.poll() == "1\n":
            break

        time.sleep(0.1)

    km.finish()