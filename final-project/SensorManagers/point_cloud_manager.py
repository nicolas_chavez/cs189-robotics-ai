import rospy
from sensor_msgs.msg import PointCloud2
import std_msgs.msg
import sensor_msgs.point_cloud2 as pcl2
import numpy as np
import time


class PointCloudManager:

    def __init__(self):

        self.pcl = None

    def start(self):

        pass

        self.pcl = rospy.Subscriber("camera/depth/points", PointCloud2, self.on_point_cloud)
        time.sleep(0.1)

    def on_point_cloud(self, point_cloud):

        # 0-640, 0-480
        points = pcl2.read_points(point_cloud, field_names=None, skip_nans=True, uvs=[(320, 479), (320, 470), (320, 460), (320, 450)])

        for p in points:

            print " x : %f  y: %f  z: %f" %(p[0],p[1],p[2])


    def local_grid_bin(self, point):

        p_x, p_y, p_z = point


if __name__ == "__main__":

    rospy.init_node('PointCloudTest', anonymous=False)
    dm = PointCloudManager()
    dm.start()

    while True:

        time.sleep(0.1)
