import rospy
from toolbox import Orientation
import numpy as np
import time
import math
import cv2

from OutputManagers.sound_manager import SoundManager
from OutputManagers.motion_manager import MotionManager

from SensorManagers.bump_manager import BumpManager
from SensorManagers.cliff_manager import CliffManager
from SensorManagers.depth_manager2 import DepthManager2
from SensorManagers.wheel_drop_manager import WheelDropManager
from SensorManagers.camera_manager import CameraManager
from SensorManagers.odom_manager import OdomManager

from Brainz.navigation_lobe2 import NavigationLobe2
from Brainz.mapmaker_lobe import MapmakerLobe
from Brainz.obstacle_lobe import ObstacleLobe
from Brainz.supernav_lobe import SupernavLobe
from Brainz.ObjectDetectors.home_detector import HomeDetector


class Buttler:

    def __init__(self):

        self.is_shutdown = False

        # Instantiate different managers

        self.motion = MotionManager()
        self.sound = SoundManager()

        self.webcam_top = CameraManager(source=CameraManager.CameraSource.WebcamTop)
        self.bump = BumpManager()
        self.cliff = CliffManager()
        self.depth = DepthManager2()
        self.wheel_drop = WheelDropManager()
        self.odometry = OdomManager()

        self.mapmaker = MapmakerLobe()
        self.navigation = NavigationLobe2()
        self.obstacle = ObstacleLobe()
        self.supernav = SupernavLobe()

        self.home_detector = HomeDetector()

        self.is_boxed_out = False

    def start(self):

        """
        Starts up the different modules.
        """

        # Initialization

        rospy.init_node('Buttler', anonymous=False)
        rospy.on_shutdown(self.shutdown)

        # Start modules

        self.motion.start()
        self.sound.start()

        self.webcam_top.start()
        self.bump.start()
        self.cliff.start()
        self.depth.start()
        self.wheel_drop.start()
        self.odometry.start()

        self.mapmaker.start()

        self.navigation.start(self.motion, self.odometry)
        self.navigation.current_pose = (0, 0, 0)

        self.supernav.start(self.navigation)

        self.obstacle.start()
        # self.obstacle.start(self.depth, self.motion)

        # shutdown if wheel dropped
        self.wheel_drop.on_drop_callback = self.shutdown

    def run(self):

        while not self.is_shutdown:

            # Determine next spot

            current_pose = self.navigation.get_current_pose()

            if self.bump.have_bumped():

                self.supernav.back_up_turn(self.bump.bump_dir)

            elif self.is_boxed_out:

                pass

            else:

                target = (3, 0, False)

                if self.webcam_top.current_image is not None:

                    im = np.copy(self.webcam_top.current_image)

                    im = cv2.resize(im, (0, 0), fx=0.3, fy=0.3)
                    im = cv2.transpose(im)
                    im = cv2.flip(im, 0)

                    rect, dist, ang = self.home_detector.detect(im)
                    HomeDetector.render_detection(im, rect, im_scale=1)
                    if dist is not None and ang is not None:
                        target = self.supernav.home_to_target((rect, dist, ang))

                _ = self.supernav.turn_to_target(target)

                if self.depth.current_image is not None:
                    n_im = np.copy(self.depth.current_image)
                    n_im = self.depth.near_sighted(n_im)
                    DepthManager2.debug_draw(self.depth.current_image)

                    presences = self.obstacle.scan(n_im)

                    obstacle_blocks = []
                    for v in presences:
                        t = True
                        if v == 1.0:
                            t = False
                        obstacle_blocks.append(t)

                    self.supernav.travel_to_target_obs(obstacle_blocks, target)

    def shutdown(self):

        """
        Stops the turtlebot's motion completely.
        """

        self.is_shutdown = True
        rospy.loginfo("Stop TurtleBot")
        self.motion.stop_moving()
        # sleep just makes sure TurtleBot receives the stop command prior to shutting down the script
        rospy.sleep(5)


if __name__ == "__main__":

    b = Buttler()
    b.start()
    b.run()
