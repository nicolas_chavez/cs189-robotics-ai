import rospy

from Modes.wander_mode import WanderMode


class HorsDOeuvres:

    def __init__(self):

        self.wander_mode = WanderMode()
        self.is_shutdown = False

    def start(self):

        rospy.init_node('HorsDOeuvres', anonymous=False)
        rospy.on_shutdown(self.shutdown)

        self.wander_mode.start()

    def run(self):

        while not self.is_shutdown:

            self.is_shutdown = self.wander_mode.update()

    def shutdown(self):

        """
        Stops the turtlebot's motion completely.
        """

        self.is_shutdown = True
        rospy.loginfo("Stop TurtleBot")
        # self.motion.stop_moving()
        # sleep just makes sure TurtleBot receives the stop command prior to shutting down the script
        rospy.sleep(5)


if __name__ == "__main__":

    hd = HorsDOeuvres()
    hd.start()
    hd.run()
