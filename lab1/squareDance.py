
import rospy
from geometry_msgs.msg import Twist
from kobuki_msgs.msg import BumperEvent
from math import radians

cmd_vel = None

# boolean variables for bump event processing
bump = False

def shutdown():

	global cmd_vel

	# stop turtlebot
	rospy.loginfo("Stop!")
	# publish a zeroed out Twist object
	cmd_vel.publish(Twist())
	# sleep before final shutdown
	rospy.sleep(1)

# If bump data is received, process the data
# data.bumper: LEFT (0), CENTER (1), RIGHT (2)
# data.state: RELEASED (0), PRESSED (1)
def processBumpSensing(data):
		# global keyword to reference variable declared outside function
		global bump

		if (data.state == BumperEvent.PRESSED):
				bump = True
		rospy.loginfo("Bumper Event")
		rospy.loginfo(data.bumper)

def rotate_left_90():

	global cmd_vel

	rate = 10

	r = rospy.Rate(rate);

	# Generate a 'left' twist object.
	turn_left = Twist()
	turn_left.linear.x = 0
	turn_left.angular.z = radians(90) #90 deg/s in radians/s

	# For 10 itterations publish a twist and sleep 1 Hz
	for i in range(0,rate):
			cmd_vel.publish(turn_left)
			r.sleep()

def move_forward(distance):

	global cmd_vel

	rate = 10

	r = rospy.Rate(rate);

	# Generate a 'left' twist object.
	move = Twist()
	move.linear.x = 0.2 #m/s
	move.angular.z = 0

	# For 10 itterations publish a twist and sleep 1 Hz
	for i in range(0,rate*distance):
			cmd_vel.publish(move)
			r.sleep()

def square_up():

	global bump
	global cmd_vel

	# initiliaze
	rospy.init_node('squareDance', anonymous=False)
	

	# What to do you ctrl + c (call shutdown function written below)
	rospy.on_shutdown(shutdown)
	
	cmd_vel = rospy.Publisher('cmd_vel_mux/input/navi', Twist, queue_size=10)
	rospy.sleep(1) # give rospy time to register the publisher
	 
	# Subscribe to queues for receiving sensory data
	rospy.Subscriber('mobile_base/events/bumper', BumperEvent, processBumpSensing)

	# 10 Hz
	r = rospy.Rate(10)

	##### INITIALIZATION MOSTLY DONE

	print "Finished Init!"

	for i in range(1,5):

		rotate_left_90()
		rospy.sleep(1)
		move_forward(2)

	# as long as you haven't ctrl + c keeping doing...
	#while not rospy.is_shutdown():

		

	"""
	if step_count > 4:
		print "Finished steps"
		break
	else:
		if bump:
			print "Bumped!"
			# STOP
			move_cmd.linear.x = 0
			move_cmd.angular.z = 0
		else: 
			# FORWARD SLOWLY
			print "Moving..."
			if forward_count < NUM_FORWARD_STEPS:
				print "Forward..."
				move_cmd.linear.x = LIN_SPEED
				move_cmd.angular.z = 0
				forward_count += 1
			else:
				print "Rotating..."
				move_cmd.linear.x = 0
				move_cmd.angular.z = ROT_SPEED

				#step_count += 1
				#forward_count = 0

		# publish the velocity
		cmd_vel.publish(move_cmd)
		# wait for 0.1 seconds (10 HZ) and publish again
		rospy.sleep(1)
		print "Next Move!"
	"""
if __name__ == "__main__":
	#try:
		#something
		square_up()
	#except:
	#	rospy.loginfo("node terminated.")