'''
Copyright (c) 2015, Mark Silliman
All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
Modified by Serena Booth
'''
import cv2
import rospy
import numpy as np

from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

class FaceDetection():

    def faceDetect(self, image):
            # TO DO: implement me!
            # Hint: the Haar classifier uses the following default arguments:
                #scaleFactor=1.1,
                #minNeighbors=5,
                #minSize=(30, 30),
                #flags=cv2.cv.CV_HAAR_SCALE_IMAGE

            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            faces = self.face_cascade.detectMultiScale(gray, 1.1, 5, cv2.cv.CV_HAAR_SCALE_IMAGE, (30, 30))
            
            for (x,y,w,h) in faces:
                cv2.rectangle(image,(x,y),(x+w,y+h),(255,0,0),2)
                #roi_gray = gray[y:y+h, x:x+w]
                #roi_color = image[y:y+h, x:x+w]

            # TO DO: draw a rectangle around all detected faces

            # Display the resulting image
            cv2.imshow('Video', image)
            cv2.waitKey(3)

    def processImage(self, data):
        image = self.bridge.imgmsg_to_cv2(data, "bgr8")
        self.faceDetect(image)

    def __init__(self):
        rospy.init_node('FaceDetection', anonymous=False)
        # ctrl + c -> call self.shutdown function
        rospy.on_shutdown(self.shutdown)
        # print msg
        rospy.loginfo("Hello World!")
        # How often should provide commands? 10 HZ
        r = rospy.Rate(10)
        self.bridge = CvBridge()

        # TO DO: read in classifier. Hint: make this an instance variable

        self.face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

        # TO DO: subscribe to the webcam image

        rospy.Subscriber('/image_raw', Image, self.processImage, queue_size=1,  buff_size=2**24)

        # do nothing.
        while not rospy.is_shutdown():
            rospy.spin()

    def shutdown(self):
        cv2.destroyAllWindows()
        rospy.loginfo("Stop")
        rospy.sleep(5)

if __name__ == '__main__':
    #try:
    FaceDetection()
    #except Exception, e:
    #print e
    #rospy.loginfo("FaceDetection node terminated.")
