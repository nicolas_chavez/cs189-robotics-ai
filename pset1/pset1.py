'''
Copyright (c) 2015, Mark Silliman
All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
Modified by Serena Booth
'''
import rospy
from geometry_msgs.msg import Twist
from kobuki_msgs.msg import BumperEvent
from math import radians
from random import randint

lin_speed = 0.3 #m/s

# boolean variable for bump event processing
bump = False

# which bumper was hit (-1, 0, 1) -> (left, center, right)
direction = None

# If bump data is received, process the data
# data.bumper: LEFT (0), CENTER (1), RIGHT (2)
# data.state: RELEASED (0), PRESSED (1)

# Registered as callback for bump events
def processBumpSensing(data):
	# global keyword to reference variable declared outside function
	global bump
	global direction

	if (data.state == BumperEvent.PRESSED):
		bump = True
		direction = data.bumper - 1
	else:
		bump = False

# Main controlling method
def GoForwardAvoidBump():

	global bump
	global cmd_vel
	global direction

	# Initialization

	rospy.init_node('GoForwardAvoidBump', anonymous=False)
	rospy.loginfo("To stop TurtleBot CTRL + C")
	rospy.on_shutdown(shutdown)
	cmd_vel = rospy.Publisher('cmd_vel_mux/input/navi', Twist, queue_size=10)
	rospy.Subscriber('mobile_base/events/bumper', BumperEvent, processBumpSensing)
	r = rospy.Rate(10);
	

	move_cmd = Twist()

	# as long as you haven't ctrl + c keeping doing...
	while not rospy.is_shutdown():

		print "Bump: %d" % bump
		if bump:
			print "Bumper: %d" % direction

		# If there is a bump, back up and turn in the appropriate direction
		if bump:
			# Back that a$$ up
			move_backward(1)
			rotate_away()
		else: 
			# Move forward otherwise
			move_cmd.linear.x = lin_speed
			move_cmd.angular.z = 0
			cmd_vel.publish(move_cmd)

		# wait for 0.1 seconds (10 HZ) and publish again
		r.sleep()

# Backs the bot up
def move_backward(distance):

	global cmd_vel

	rate = 5

	r = rospy.Rate(rate);

	# Generate a 'left' twist object.
	move = Twist()
	move.linear.x = -0.3 #m/s
	move.angular.z = 0

	# For 10 iterations publish a twist and sleep 1 Hz
	for i in range(0,rate*distance):
		cmd_vel.publish(move)
		r.sleep()

# Rotates the bot away, based on which bumper was hit
def rotate_away():

	global cmd_vel
	global direction

	# Fast turning speed
	turn_speed = 200

	# turn other way if left bumper
	if direction == -1:
		turn_speed *= -1

	rate = 50

	r = rospy.Rate(rate);

	# Generate a 'left' twist object.
	turn_left = Twist()
	turn_left.linear.x = 0
	turn_left.angular.z = radians(turn_speed) #90 deg/s in radians/s

	# add some randnomness to how much to turn exactly
	iterations = randint(20,rate)
	for i in range(0, iterations):
		cmd_vel.publish(turn_left)
		r.sleep()		
		
def shutdown():

	global cmd_vel

	# stop turtlebot
	rospy.loginfo("Stop TurtleBot")
	# a default Twist has linear.x of 0 and angular.z of 0.  So it'll stop TurtleBot
	cmd_vel.publish(Twist())
	# sleep just makes sure TurtleBot receives the stop command prior to shutting down the script
	rospy.sleep(5)
 
if __name__ == '__main__':

	GoForwardAvoidBump()
	#try:
		#GoForwardAvoidBump()
	#except:
		#rospy.loginfo("GoForwardAvoidBump node terminated.")
