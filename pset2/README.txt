Our strategy for this assignment was to determine which part of the image plane returned by the depth sensor is the "best" part to proceed through.

We applied a crop to the depth image to ignore the robot’s own poles and also objects above the robots height. We blurred the image to reduce high-frequency noise.

To do this, we first considered only the depth information from 0 to 1.2 since this would work with objects that were close to the robot. Also this data tends to be more reliable, as the sensor is able to elucidate close objects more easily.

To find this, we discretized the image into a number of vertical (12 in our case) segments and found the "best" continuous 5-segment (corresponding part of the image without objects). To do this, we averaged the pixel intensity values within each segment, and then convolved a 1D-filter across the vector of means to find the continuous section. We weighted NaN (unable to get depth) with 0.5 weighting and other objects according to how close it is.

We know know which section of our image shows a clear path and we move toward that direction by returning the segment value where left side is -1 and right side is 1, center is 0, with a smooth function in between. 

We then use this multiplier to proportionally calculate our angular velocity! One major issue we had is that objects disappear due to the proximity blind spot, so we introduce a time delay between sensing and action.