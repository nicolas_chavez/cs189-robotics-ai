'''
Copyright (c) 2015, Mark Silliman
All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
Modified by Serena Booth
'''

import math
import cv2
import rospy
import numpy as np 
from geometry_msgs.msg import Twist
from kobuki_msgs.msg import BumperEvent, CliffEvent, WheelDropEvent
from sensor_msgs.msg import PointCloud2, LaserScan, Image
from cv_bridge import CvBridge, CvBridgeError
from math import radians
from random import randint

import time

# boolean variables for bump event processing
bump = False

lin_speed = 0.2 #m/s

# which bumper was hit (-1, 0, 1) -> (left, center, right)
direction = None

kinect_scan_result = 0

bridge = None

def processDepthImage(data):

    global kinect_scan_result
    global bridge

    #try: 
    # Use bridge to convert to CV::Mat type. 
    # 8UC1 is the image format
    cv_image = bridge.imgmsg_to_cv2(data, '8UC1')
    # Get depth information as an array 
    depth_array = np.array(cv_image, dtype=np.float32)
    # Convert values to range between 0 and 1

    depth_array = black_fill2(depth_array)

    cv2.normalize(depth_array, depth_array, 0, 1, cv2.NORM_MINMAX)


    # remove 70 px from left, 40 px from right, 20 px from bottom, 10 px from top

    # Remember to apply a blur! 
    # If desired, convert to familiar BGR colorspace for segementation
    # but you may use inRange directly on the gray image, depth_array 
    #depth_array = cv2.cvtColor(depth_array, cv2.COLOR_GRAY2BGR)
    # Threshold here!


    # Blur
    # = cv2.GaussianBlur(depth_array, (5, 5), 0)
    
    cropped = depth_array[10:460, 70:600]

    #cropped = cv2.cvtColor(cropped, cv2.COLOR_GRAY2BGR)

    kinect_scan_result = findClearPath(cropped)

    #print kinect_scan_result

    cv2.imshow('Depth Image', cropped)
    cv2.waitKey(3)
    #except CvBridgeError, e: 
        #rospy.loginfo(e)

    """
    cur_millis = int(round(time.time() * 1000))
    if self.last_millis != 0:
        diff = cur_millis - self.last_millis
        fps = int(round((1000.0 / diff)))
        print fps

    self.last_millis = cur_millis
    """

def findClearPath(image):
    # discretizes the image into 10 vertical stripes
    # returns which stripe has the clearest path
    
    _, black_mask = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY)
    black_mask = black_mask.astype(np.uint8)

    im_h, im_w, im_d = image.shape
    num_segments = 5
    segment_size = float(im_w) / num_segments
    segment_size = int(math.floor(segment_size))

    means = np.zeros((1,num_segments))

    for i in range(num_segments):

        left_edge = segment_size*i
        right_edge = segment_size*(i + 1)

        segment = image[0:im_h, left_edge:right_edge, 0]
        mask_seg = black_mask[0:im_h, left_edge:right_edge]


        mean = cv2.mean(np.power(segment, 0.25), mask_seg)



        means[0, i] = mean[0]

        print means

    seg_num = np.argmin(means)

    return float(seg_num - (num_segments/2))/float(num_segments/2)

def black_fill(image):

    h, w, c = image.shape
    # 450 530 1

    # go from top to bottom, left to right fill black pixels with ones from above

    BLACK_VAL = 0.0
    DEFAULT_VAL = 0.5

    above_val = DEFAULT_VAL

    for col in range(w):
        for row in range(h):

            cur_val = image[row, col, 0]

            if cur_val == BLACK_VAL:
                cur_val = above_val
                image[row, col, 0] = cur_val

            above_val = cur_val

        above_val = DEFAULT_VAL

    return image

def black_fill2(image):

    h, w, c = image.shape
    
    _, black_mask = cv2.threshold(image, 0, 1, cv2.THRESH_BINARY_INV)

    image[:,:,0] = image[:,:,0] + black_mask

    return image


# If bump data is received, process the data
# data.bumper: LEFT (0), CENTER (1), RIGHT (2)
# data.state: RELEASED (0), PRESSED (1)
# Registered as callback for bump events
def processBumpSensing(data):
    # global keyword to reference variable declared outside function
    global bump
    global direction

    if (data.state == BumperEvent.PRESSED):
        bump = True
        direction = data.bumper - 1
    else:
        bump = False


def GoForwardAvoidBump():

    global bump
    global cmd_vel
    global direction
    global kinect_scan_result
    global bridge

    # Use a CvBridge to convert ROS image type to CV Image (Mat)
    bridge = CvBridge()

    # Initialization

    rospy.init_node('GoForwardAvoidBump', anonymous=False)
    rospy.loginfo("To stop TurtleBot CTRL + C")
    rospy.on_shutdown(shutdown)
    cmd_vel = rospy.Publisher('cmd_vel_mux/input/navi', Twist, queue_size=10)
    rospy.Subscriber('mobile_base/events/bumper', BumperEvent, processBumpSensing)
    # Subscribe to depth topic 
    rospy.Subscriber('/camera/depth/image', Image, processDepthImage, queue_size=1,  buff_size=2**24)
    r = rospy.Rate(10);

    move_cmd = Twist()

    # as long as you haven't ctrl + c keeping doing...
    while not rospy.is_shutdown():

        """
        print "Bump: %d" % bump
        """
        if bump:
            print "Bumper: %d" % direction
        

        # If there is a bump, back up and turn in the appropriate direction
        if bump:
            # Back that a$$ up
            move_backward(1)
            rotate_away()
        else: 
            # Move forward otherwise
            ang_vel = (0.7 * kinect_scan_result)
            #print ang_vel
            move_cmd.linear.x = lin_speed
            move_cmd.angular.z = ang_vel
            cmd_vel.publish(move_cmd)

        # wait for 0.1 seconds (10 HZ) and publish again
        r.sleep()

    

    """
    self.last_millis = 0

    rospy.init_node('Scan', anonymous=False)

    # tell user how to stop TurtleBot
    rospy.loginfo("To stop TurtleBot CTRL + C")

    # What function to call when you ctrl + c    
    rospy.on_shutdown(self.shutdown)
    """

    # Create a publisher which can "talk" to TurtleBot and tell it to move
    #self.cmd_vel = rospy.Publisher('cmd_vel_mux/input/navi', Twist, queue_size=10)

# Backs the bot up
def move_backward(distance):

    global cmd_vel

    rate = 5

    r = rospy.Rate(rate);

    # Generate a 'left' twist object.
    move = Twist()
    move.linear.x = -0.3 #m/s
    move.angular.z = 0

    # For 10 iterations publish a twist and sleep 1 Hz
    for i in range(0,rate*distance):
        cmd_vel.publish(move)
        r.sleep()

# Rotates the bot away, based on which bumper was hit
def rotate_away():

    global cmd_vel
    global direction

    # Fast turning speed
    turn_speed = 200

    # turn other way if left bumper
    if direction == -1:
        turn_speed *= -1

    rate = 50

    r = rospy.Rate(rate);

    # Generate a 'left' twist object.
    turn_left = Twist()
    turn_left.linear.x = 0
    turn_left.angular.z = radians(turn_speed) #90 deg/s in radians/s

    # add some randnomness to how much to turn exactly
    iterations = randint(20,rate)
    for i in range(0, iterations):
        cmd_vel.publish(turn_left)
        r.sleep()   
                        
def shutdown():

    global cmd_vel

    # stop turtlebot
    rospy.loginfo("Stop TurtleBot")
    # a default Twist has linear.x of 0 and angular.z of 0.  So it'll stop TurtleBot
    cmd_vel.publish(Twist())
    # sleep just makes sure TurtleBot receives the stop command prior to shutting down the script
    rospy.sleep(5)

if __name__ == '__main__':
    #try:
    GoForwardAvoidBump()
    #except Exception as e:
    #print type(e)
    #rospy.loginfo("KinectScan node terminated.")
