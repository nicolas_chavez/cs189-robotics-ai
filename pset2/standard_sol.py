"""
Copyright (c) 2015, Mark Silliman
All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
Modified by Serena Booth
"""

import math
import time
import cv2
import rospy
import numpy as np 
from geometry_msgs.msg import Twist
from kobuki_msgs.msg import BumperEvent, CliffEvent, WheelDropEvent
from sensor_msgs.msg import PointCloud2, LaserScan, Image
from cv_bridge import CvBridge, CvBridgeError
from math import radians
from random import randint


# boolean variables for bump event processing
bump = False

lin_speed = 0.2  # m/s

# which bumper was hit (-1, 0, 1) -> (left, center, right)
direction = None

# Value from -1 to 1 indicating best (emptiest) horizontal space along axis to proceed through
kinect_scan_result = 0

bridge = None


def process_depth_image(data):

    global kinect_scan_result
    global bridge

    # Use bridge to convert to CV::Mat type.
    # NOTE: SOME DATA WILL BE 'NaN'
    # and numbers correspond to distance to camera in meters
    cv_image = bridge.imgmsg_to_cv2(data, 'passthrough')

    # manipulate the image directly here!

    # remove 70 px from left, 40 px from right, 20 px from bottom, 10 px from top
    cv_image_cropped = cv_image[10:460, 70:600]

    # Blur
    cv_image_cropped = cv2.GaussianBlur(cv_image_cropped, (7, 7), 3)

    # Convert to BGR
    cv_image_cropped = cv2.cvtColor(cv_image_cropped, cv2.COLOR_GRAY2BGR)

    # Replace Nans with 0.5, an intermediate value
    cv_image_cropped[np.isnan(cv_image_cropped)] = 0.5

    # Masks out farther stuff, only close things important
    lower_bound = np.array([0.0, 0.0, 0.0])
    upper_bound = np.array([1.2, 1.2, 1.2])

    close_mask = cv2.inRange(cv_image_cropped, lower_bound, upper_bound)
    masked = cv2.bitwise_and(cv_image_cropped, cv_image_cropped, mask=close_mask)

    kinect_scan_result = find_clear_path(masked)

    # laplaced = cv2.Laplacian(masked, ddepth=-1, dst=None, ksize=3, scale=1.0, delta=0.0, borderType=cv2.BORDER_DEFAULT)

    # canny = cv2.Canny(masked[:, :, 0], 0, 2, edges=None, apertureSize=3)
    # make a display copy, rather than pointers

    # Draw a circle to where best space to move in is
    cv_image_display = np.copy(masked)

    x_ctr = (kinect_scan_result / 2.0) + 0.5
    x_ctr *= cv_image_display.shape[1]
    x_ctr = int(x_ctr)
    y_ctr = cv_image_display.shape[0]/2

    cv2.circle(cv_image_display, (x_ctr, y_ctr), 20, (255, 0, 0), thickness=3)

    """
    # Convert values to range between 0 and 1.
    # Normalize ONLY to view the picture, not to avoid obstacles!
    cv2.normalize(cv_image_display, cv_image_display, 0, 1, cv2.NORM_MINMAX)
    cv_image_display *= 255
    cv_image_display = cv_image_display.astype(np.uint8)

    canny = cv2.Canny(cv_image_display[:, :, 0], 0, 100, edges=None, apertureSize=3)
    cv2.imshow('Depth Image', canny)
    """
    cv2.imshow('Actual', cv_image_display)
    cv2.waitKey(3)

    """
    # FPS info
    cur_millis = int(round(time.time() * 1000))
    if self.last_millis != 0:
        diff = cur_millis - self.last_millis
        fps = int(round((1000.0 / diff)))
        print fps

    self.last_millis = cur_millis
    """


def find_clear_path(image):
    # discretizes the image into 10 vertical stripes
    # returns which stripe has the clearest path

    im_h, im_w, im_d = image.shape
    num_segments = 10
    segment_size = float(im_w) / num_segments
    segment_size = int(math.floor(segment_size))

    # calculate mean pixel intesity non each segment
    means = np.zeros((1, num_segments))

    nothing_thresh = 0.25
    below_threshold = True

    for i in range(num_segments):

        left_edge = segment_size*i
        right_edge = segment_size*(i + 1)

        segment = image[0:im_h, left_edge:right_edge, 0]

        mean = cv2.mean(segment)
        mean = mean[0]

        means[0, i] = mean

        if mean > nothing_thresh:
            below_threshold = False

    # if most of the image is blank, then stay centered
    if below_threshold:
        return 0.0

    # convolve
    # Do this to make sure that our "open space" is 3 segments wide
    # which is what the robot will fit through
    kernel = np.array([[2.0, 1.0, 2.0]])
    m = np.array(means)
    conv = cv2.filter2D(m, -1, kernel, borderType=cv2.BORDER_REPLICATE)

    # return number from -1 to 1
    seg_num = np.argmin(conv)

    return float(seg_num - (num_segments/2))/float(num_segments/2)


# If bump data is received, process the data
# data.bumper: LEFT (0), CENTER (1), RIGHT (2)
# data.state: RELEASED (0), PRESSED (1)
# Registered as callback for bump events
def process_bumping_sensing(data):
    # global keyword to reference variable declared outside function
    global bump
    global direction

    if data.state == BumperEvent.PRESSED:
        bump = True
        direction = data.bumper - 1
    else:
        bump = False


def go_forward_avoid_bump():

    global bump
    global cmd_vel
    global direction
    # global kinect_scan_result
    global bridge

    # Use a CvBridge to convert ROS image type to CV Image (Mat)
    bridge = CvBridge()

    # Initialization

    rospy.init_node('GoForwardAvoidBump', anonymous=False)
    rospy.loginfo("To stop TurtleBot CTRL + C")
    rospy.on_shutdown(shutdown)
    cmd_vel = rospy.Publisher('cmd_vel_mux/input/navi', Twist, queue_size=10)
    rospy.Subscriber('mobile_base/events/bumper', BumperEvent, process_bumping_sensing)
    # Subscribe to depth topic
    rospy.Subscriber('/camera/depth/image', Image, process_depth_image, queue_size=1, buff_size=2 ** 24)
    r = rospy.Rate(10)

    move_cmd = Twist()

    # as long as you haven't ctrl + c keeping doing...
    while not rospy.is_shutdown():

        """
        print "Bump: %d" % bump
        """
        if bump:
            print "Bumper: %d" % direction

        # If there is a bump, back up and turn in the appropriate direction
        if bump:
            # Back that a$$ up
            move_backward(1)
            rotate_away()
        else:
            # Move forward otherwise
            ang_vel = (-1 * kinect_scan_result)
            # print ang_vel
            move_cmd.linear.x = lin_speed
            move_cmd.angular.z = ang_vel
            cmd_vel.publish(move_cmd)

        # wait for 0.1 seconds (10 HZ) and publish again
        r.sleep()

    """
    self.last_millis = 0

    rospy.init_node('Scan', anonymous=False)

    # tell user how to stop TurtleBot
    rospy.loginfo("To stop TurtleBot CTRL + C")

    # What function to call when you ctrl + c
    rospy.on_shutdown(self.shutdown)
    """


# Backs the bot up
def move_backward(distance):

    global cmd_vel

    rate = 5

    r = rospy.Rate(rate)

    # Generate a 'left' twist object.
    move = Twist()
    move.linear.x = -0.3  # m/s
    move.angular.z = 0

    # For 10 iterations publish a twist and sleep 1 Hz
    for i in range(0, rate*distance):
        cmd_vel.publish(move)
        r.sleep()


# Rotates the bot away, based on which bumper was hit
def rotate_away():

    global cmd_vel
    global direction

    # Fast turning speed
    turn_speed = 200

    # turn other way if left bumper
    if direction == -1:
        turn_speed *= -1

    rate = 50

    r = rospy.Rate(rate)

    # Generate a 'left' twist object.
    turn_left = Twist()
    turn_left.linear.x = 0
    turn_left.angular.z = radians(turn_speed)  # 90 deg/s in radians/s

    # add some randomness to how much to turn exactly
    iterations = randint(20, rate)
    for i in range(0, iterations):
        cmd_vel.publish(turn_left)
        r.sleep()


def shutdown():

    global cmd_vel

    # stop turtlebot
    rospy.loginfo("Stop TurtleBot")
    # a default Twist has linear.x of 0 and angular.z of 0.  So it'll stop TurtleBot
    cmd_vel.publish(Twist())
    # sleep just makes sure TurtleBot receives the stop command prior to shutting down the script
    rospy.sleep(5)

if __name__ == '__main__':

    go_forward_avoid_bump()
