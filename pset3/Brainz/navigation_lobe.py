
import cv2
import numpy as np
import math

from SensorManagers.camera_manager import CameraManager


class NavigationLobe:

    def __init__(self):

        self.started = False
        self.boxed_out = False
        self.num_segments = 12

        self.debug_rect = None

        delay = 15  # number of frames delay between sight and action (fps = 30)

        self.route_decisions = np.zeros(delay)

        # To inform about being boxed out
        self.boxed_out_callback = None

        # found obj -> (242, 188, 55, 151) cone

    def start(self):

        self.started = True

    def route_decision(self):

        return self.route_decisions[0]

    def check_target_proximity(self, depth_image, target_bounding_box, target_kind):

        im_h, im_w, _ = depth_image.shape

        t_x, t_y, t_w, t_h = target_bounding_box
        ctr_x = t_x + t_w/2

        if target_kind == CameraManager.BOTTLE_OBJ:
            size_lb = 1000
            bottom_trigger = 100
        if target_kind == CameraManager.CONE_OBJ:
            size_lb = 2500
            bottom_trigger = 5
        if target_kind == CameraManager.CUBE_OBJ:
            size_lb = 3000
            bottom_trigger = 5

        left_x_bound = im_w/4
        right_x_bound = 3*im_w/4

        return im_h - (t_y + t_h) < bottom_trigger and size_lb < t_w * t_h and left_x_bound <= ctr_x <= right_x_bound


    def depth_image_means(self, depth_image):

        # discretizes the image into 10 vertical stripes
        # returns which stripe has the clearest path

        im_h, im_w, im_d = depth_image.shape
        num_segments = 12
        segment_size = float(im_w) / num_segments
        segment_size = int(math.floor(segment_size))

        # calculate mean pixel intesity non each segment
        means = np.zeros((self.num_segments))

        nothing_thresh = 0.25
        below_threshold = True

        for i in range(num_segments):

            left_edge = segment_size*i
            right_edge = segment_size*(i + 1)

            segment = depth_image[0:im_h, left_edge:right_edge, 0]

            # square root so that closer things are more cost
            mean = cv2.mean(np.sqrt(segment))
            mean = mean[0]

            means[i] = mean

            if mean > nothing_thresh:
                below_threshold = False

        return means, below_threshold

    def route_decide(self, depth_image, target_bounding_box=None, target_distance=None, target_kind=None):

        self.boxed_out = False

        means, below_threshold = self.depth_image_means(depth_image)

        # If too much going on, freak out
        meanmedian = np.median(means)
        self.boxed_out = (meanmedian > 0.65)

        self.debug_rect = target_bounding_box

        found_obj = False

        if target_bounding_box is not None:

            found_obj = self.check_target_proximity(depth_image, target_bounding_box, target_kind)

            _, im_w, _ = depth_image.shape
            x, y, w, h = target_bounding_box

            center_x = x + (w/2)
            target_seg_num = int(center_x) / int(float(im_w) / self.num_segments)

            # set 3 around to 0
            for i in range(target_seg_num - 2, target_seg_num + 3):

                if 0 <= i < self.num_segments:
                    if i < target_seg_num - 1:
                        means[i] = 0.2
                    elif i > target_seg_num + 1:
                        means[i] = 0.2
                    else:
                        means[i] = 0.0

        route_decision = 0.0

        # If most of the image is blank, then stay centered
        if not below_threshold:

            # convolve
            # Do this to make sure that our "open space" is 3 segments wide
            # which is what the robot will fit through
            kernel = np.array([[2.0, 1.0, 1.0, 1.0, 2.0]])
            m = np.array([means])
            conv = cv2.filter2D(m, -1, kernel, borderType=cv2.BORDER_REPLICATE)

            conv = conv[0]
            # return number from -1 to 1
            seg_num = np.argmin(conv)
            route_decision = float(seg_num - (self.num_segments/2))/float(self.num_segments/2)

        self.route_decisions = np.roll(self.route_decisions, -1)
        self.route_decisions[-1] = route_decision

        self.draw_route_decision(depth_image, None, means)

        if self.boxed_out and self.boxed_out_callback is not None:

            self.boxed_out_callback(np.sign(route_decision))

        return route_decision, means, found_obj

    def draw_route_decision(self, image, draw_rect, means):

        seg_height = 40

        if draw_rect is None:

            d_x = 0
            d_y = 0
            d_h, d_w, _ = image.shape

        else:
            d_x, d_y, d_w, d_h = draw_rect

        segment_size = float(d_w) / self.num_segments
        segment_size = int(math.floor(segment_size))

        means -= np.amin(means)
        means /= np.amax(means)

        for i in range(self.num_segments):

            left_edge = segment_size*i

            r = int(255.0 * means[i])
            b = 255 - r

            cv2.rectangle(image, (d_x + left_edge, d_y), (d_x + left_edge + segment_size, d_y + int(seg_height * (1-means[i]))), (b, 0, r), thickness=-1)

        # Draw a circle to where best space to move in is
        x_ctr = (self.route_decisions[-1] / 2.0) + 0.5
        x_ctr *= image.shape[1]
        x_ctr = int(x_ctr)
        y_ctr = image.shape[0]/2

        cv2.circle(image, (x_ctr, y_ctr), 20, (255, 0, 100), thickness=3)

        if self.debug_rect is not None:
            r_x, r_y, r_w, r_h = self.debug_rect
            cv2.rectangle(image, (r_x, r_y), (r_x+r_w, r_y+r_h), (255, 0, 0), thickness=3)

        cv2.imshow('Actual', image)
        cv2.waitKey(3)
