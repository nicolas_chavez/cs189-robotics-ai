
import rospy

from geometry_msgs.msg import Twist

import numpy as np
from math import radians
from random import randint

# Module in charge of applying motion


class MotionManager:

    def __init__(self):

        # The cmd to use to publish
        self.cmd_vel = rospy.Publisher('cmd_vel_mux/input/navi', Twist, queue_size=10)
        self.lin_speed = 0.12  # m/s
        self.started = False

    def start(self):

        self.started = True

    # Step forward with a given speed
    def move_forward(self, lin_vel=None, ang_vel=None):

        if self.started:
            if lin_vel is None:
                lin_vel = self.lin_speed

            if ang_vel is None:
                ang_vel = 0

            move_cmd = Twist()

            move_cmd.linear.x = lin_vel
            move_cmd.angular.z = ang_vel
            self.cmd_vel.publish(move_cmd)

    # Step back with a given distance
    def move_backward(self, distance=None):

        if self.started:
            if distance is None:
                distance = 1.0

            rate = 5

            r = rospy.Rate(rate)

            # Generate a 'left' twist object.
            move = Twist()
            move.linear.x = -0.3
            move.angular.z = 0

            # For 10 iterations publish a twist and sleep 1 Hz
            for i in range(0, int(rate*distance)):
                self.cmd_vel.publish(move)
                r.sleep()

    # Rotates the bot slowly..
    def rotate_slow(self):

        if self.started:
            rate = 10

            # Slow turning speed
            turn_speed = 50

            r = rospy.Rate(rate)

            # Generate a 'left' twist object.
            turn_left = Twist()
            turn_left.linear.x = 0
            turn_left.angular.z = radians(turn_speed)  # 90 deg/s in radians/s

            # add some randomness to how much to turn exactly
            for i in range(0, 10):
                self.cmd_vel.publish(turn_left)
                r.sleep()

    # Rotates the bot away, based on which bumper was hit
    def rotate_away(self, direction):

        if self.started:
            # Fast turning speed
            turn_speed = 200 * np.sign(direction)

            rate = 50

            r = rospy.Rate(rate)

            # Generate a 'left' twist object.
            turn_left = Twist()
            turn_left.linear.x = 0
            turn_left.angular.z = radians(turn_speed)  # 90 deg/s in radians/s

            # add some randomness to how much to turn exactly
            iterations = randint(20, rate)
            for i in range(0, iterations):
                self.cmd_vel.publish(turn_left)
                r.sleep()

    # Rotates by a small amount, slowly and cautiously
    def rotate_tiny_bit(self, direction):

        if self.started:
            # Slow turning speed
            turn_speed = 60 * np.sign(direction)

            rate = 30

            r = rospy.Rate(rate)

            # Generate a twist object.
            turn = Twist()
            turn.linear.x = 0
            turn.angular.z = radians(turn_speed)  # 90 deg/s in radians/s

            # add some randomness to how much to turn exactly
            iterations = 20
            for i in range(0, iterations):
                self.cmd_vel.publish(turn)
                r.sleep()

    # Stop motions
    def stop_moving(self):

        if self.started:
            # a default Twist has linear.x of 0 and angular.z of 0.  So it'll stop TurtleBot
            self.cmd_vel.publish(Twist())
