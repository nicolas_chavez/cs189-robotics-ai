
import cv2
import numpy as np
import os


from object_detector import ObjectDetector


# Bottle detection algorithms


class BottleDetector(ObjectDetector):

    def __init__(self, ref_im_filenames):

        ObjectDetector.__init__(self)

        # Set aspect ratio and size thresholding values
        self.size_threshold = 450
        self.aspect_ratio_lb = 1
        self.aspect_ratio_ideal = 1.4
        self.aspect_ratio_ub = 3
        self.val_threshold = 50

        self.adaptive_padding_x = 45
        self.adaptive_padding_y = 65

        self.output_dir = 'Photos/Bottle/Match/'

        # Reference/model histrograms for backprojection
        self.ref_hists = []

        # Load and create histograms
        for ref_im_filename, mask_im_filename, weight in ref_im_filenames:

            # image to search with
            ref_im = cv2.imread(ref_im_filename)
            mask_im = cv2.imread(mask_im_filename)
            ref_hist = self.create_ref_hist(ref_im, mask_im, weight)
            self.ref_hists.append(ref_hist)

    # Do backprojection thresholding
    def create_bottle_threshold(self, query_im):

        query_hsv = cv2.cvtColor(query_im, cv2.COLOR_BGR2HSV)

        # apply backprojections

        im_h, im_w, im_d = query_im.shape

        # Do multiple weighted backprojections
        master_count = np.zeros((im_h, im_w), np.uint8)

        for i in range(len(self.ref_hists)):

            ref_hist, weight = self.ref_hists[i]

            backproj = cv2.calcBackProject([query_hsv], [0, 1], ref_hist, [0, 180, 0, 256], 1)

            # size from 7 to 11
            kernel = np.ones((9, 9), np.uint8)
            backproj = cv2.morphologyEx(backproj, cv2.MORPH_CLOSE, kernel)

            # size from 7+ should work
            kernel = np.ones((15, 15), np.uint8)
            backproj = cv2.morphologyEx(backproj, cv2.MORPH_OPEN, kernel)

            kernel = np.ones((15, 15), np.uint8)
            backproj = cv2.morphologyEx(backproj, cv2.MORPH_CLOSE, kernel)

            _, threshold = cv2.threshold(backproj, 70, 255, cv2.THRESH_BINARY)

            master_count = master_count + (threshold * weight).astype(np.uint8)

        # Combine into master thresholding
        _, master_thresh = cv2.threshold(master_count, 45, 255, cv2.THRESH_BINARY)

        return master_thresh

    # Kickoff detection
    def detect(self, query_im=None, query_im_filename=None, write_out=False):

        # If in test or realtime mode
        if query_im_filename is not None:
            # image to query
            query_im = cv2.imread(query_im_filename)

        if query_im is None:
            raise Exception('Both inputs were None')

        # create threshold
        threshold = self.create_bottle_threshold(query_im)

        # examine threshold for shapes that could be bottle

        bounding_rectangle, contours = self.find_rect_in_threshold(query_im, threshold)
        a_rect = None  # self.adaptive_threshold_pass(query_im, bounding_rectangle)

        # draw results

        cv2.drawContours(query_im, contours, -1, (0, 255, 0), thickness=1)

        # choose better box

        if bounding_rectangle is not None:

            b_x, b_y, b_w, b_h = bounding_rectangle

            if a_rect is not None:
                a_x, a_y, a_w, a_h = a_rect

                if a_w * a_h > b_w * b_h:

                    b_x = a_x
                    b_y = a_y
                    b_w = a_w
                    b_h = a_h

            cv2.rectangle(query_im, (b_x, b_y), (b_x+b_w, b_y+b_h), (255, 0, 0), thickness=2)

        if write_out:
            out = query_im
            fn = os.path.basename(query_im_filename)
            cv2.imwrite(self.output_dir + fn, out)

        h, w, _ = query_im.shape
        estimated_distance = self.estimate_distance((w, h), bounding_rectangle)

        return bounding_rectangle, estimated_distance
        # check if bounding box big enough


if __name__ == "__main__":

    # If running standalone
    # The tuple contains the reference image, the mask image, and the weight to assign
    bottle_refs = [

        ('Photos/Bottle2/53.png', 'Photos/Bottle2/53-mask2.png', 0.1),
        ('Photos/Bottle2/54.png', 'Photos/Bottle2/54-mask2.png', 0.1),
        ('Photos/Bottle2/55.png', 'Photos/Bottle2/55-mask2.png', 0.1),
        ('Photos/Bottle2/56.png', 'Photos/Bottle2/56-mask2.png', 0.1),
        ('Photos/Bottle2/57.png', 'Photos/Bottle2/57-mask2.png', 0.1),
        ('Photos/Bottle2/58.png', 'Photos/Bottle2/58-mask2.png', 0.1),
        ('Photos/Bottle2/59.png', 'Photos/Bottle2/59-mask2.png', 0.1),
        ('Photos/Bottle2/60.png', 'Photos/Bottle2/60-mask2.png', 0.1),
        ('Photos/Bottle2/61.png', 'Photos/Bottle2/61-mask2.png', 0.1),
        ('Photos/Bottle2/0.png', 'Photos/Bottle2/0-mask2.png', 0.1),
    ]
    bd = BottleDetector(bottle_refs)
    bd.output_dir = 'Photos/Bottle2/Match/'

    for i in range(62):

        filename = 'Photos/Bottle2/' + str(i) + '.png'
        _, _ = bd.detect(query_im_filename=filename)

        # 17, 27, 29, 30, 31, 32, 49, 52,
