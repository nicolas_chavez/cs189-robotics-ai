
import cv2
import numpy as np
import os

from object_detector import ObjectDetector

# Cone detection algorithms


class ConeDetector(ObjectDetector):

    def __init__(self, ref_im_filenames):

        ObjectDetector.__init__(self)

        # Set size and aspect ratio thresholding values
        self.size_threshold = 450
        self.aspect_ratio_lb = 0.2
        self.aspect_ratio_ideal = 0.6
        self.aspect_ratio_ub = 1
        self.val_threshold = 60

        self.adaptive_padding_x = 45
        self.adaptive_padding_y = 60

        self.output_dir = 'Photos/Cone/Match/'

        # Reference histograms
        self.ref_hists = []

        # Load and create references/model histograms
        for ref_im_filename, mask_im_filename, weight in ref_im_filenames:

            # image to search with
            ref_im = cv2.imread(ref_im_filename)
            mask_im = cv2.imread(mask_im_filename)
            ref_hist = self.create_ref_hist(ref_im, mask_im, weight)
            self.ref_hists.append(ref_hist)

    # Does backprojection thresholding
    def create_cone_threshold(self, query_im):

        # Convert to HSV
        query_hsv = cv2.cvtColor(query_im, cv2.COLOR_BGR2HSV)

        # apply backprojections

        im_h, im_w, im_d = query_im.shape

        # Do weighted bakcprojection with multiple histograms
        master_count = np.zeros((im_h, im_w), np.uint8)

        for ref_hist, weight in self.ref_hists:

            backproj = cv2.calcBackProject([query_hsv], [0, 1], ref_hist, [0, 180, 0, 256], 1)

            # size from 7 to 11
            kernel = np.ones((9, 9), np.uint8)
            backproj = cv2.morphologyEx(backproj, cv2.MORPH_CLOSE, kernel)

            # size from 7+ should work
            kernel = np.ones((15, 15), np.uint8)
            backproj = cv2.morphologyEx(backproj, cv2.MORPH_OPEN, kernel)

            kernel = np.ones((15, 15), np.uint8)
            backproj = cv2.morphologyEx(backproj, cv2.MORPH_CLOSE, kernel)

            _, threshold = cv2.threshold(backproj, 100, 255, cv2.THRESH_BINARY)

            master_count = master_count + (threshold * weight).astype(np.uint8)

        # Combine into master threshold
        _, master_thresh = cv2.threshold(master_count, 45, 255, cv2.THRESH_BINARY)

        return master_thresh

    # Kickoff object detection
    def detect(self, query_im=None, query_im_filename=None, write_out=False):

        # If in realtime or test mode
        if query_im_filename is not None:
            # image to query
            query_im = cv2.imread(query_im_filename)

        if query_im is None:
            raise Exception('Both inputs were None')

        # create threshold
        threshold = self.create_cone_threshold(query_im)

        # examine threshold for shapes that could be cone
        # Do adaptive pass as well

        bounding_rectangle, contours = self.find_rect_in_threshold(query_im, threshold)
        a_rect = self.adaptive_threshold_pass(query_im, bounding_rectangle)

        # draw results

        cv2.drawContours(query_im, contours, -1, (0, 255, 0), thickness=1)

        # choose better box

        if bounding_rectangle is not None:

            b_x, b_y, b_w, b_h = bounding_rectangle

            # If adaptive bounding box is better
            if a_rect is not None:
                a_x, a_y, a_w, a_h = a_rect

                if a_w * a_h > b_w * b_h:

                    b_x = a_x
                    b_y = a_y
                    b_w = a_w
                    b_h = a_h

            cv2.rectangle(query_im, (b_x, b_y), (b_x+b_w, b_y+b_h), (255, 0, 200), thickness=2)

        if write_out:
            out = query_im
            fn = os.path.basename(query_im_filename)
            cv2.imwrite(self.output_dir + fn, out)

        h, w, _ = query_im.shape
        estimated_distance = self.estimate_distance((w, h), bounding_rectangle)

        return bounding_rectangle, estimated_distance
        # check if bounding box big enough


if __name__ == "__main__":

    # If running standalone
    # The tuple contains the reference image, the mask image, and the weight to assign
    cone_refs = [
        ('Photos/Cone/43.png', 'Photos/Cone/43-mask.png', 0.5),
        ('Photos/Cone/11.png', 'Photos/Cone/11-mask.png', 0.2),
        ('Photos/Cone/14.png', 'Photos/Cone/14-mask.png', 0.2),
        ('Photos/Cone/22.png', 'Photos/Cone/22-mask.png', 0.05),
        ('Photos/Cone/58.png', 'Photos/Cone/58-mask.png', 0.05)
    ]
    cd = ConeDetector(cone_refs)
    cd.output_dir = 'Photos/Cone/Match/'

    for i in range(59):

        filename = 'Photos/Cone/' + str(i) + '.png'
        _, _ = cd.detect(query_im_filename=filename)
