
import cv2
import numpy as np
import os

from object_detector import ObjectDetector

# Searches for cube in images


class CubeDetector(ObjectDetector):

    def __init__(self, ref_im_filenames):

        ObjectDetector.__init__(self)

        # Set size and aspect ratio values
        self.size_threshold = 450
        self.aspect_ratio_lb = 0.7
        self.aspect_ratio_ideal = 1.1
        self.aspect_ratio_ub = 2
        self.val_lb = 40
        self.val_ub = 220

        self.adaptive_padding_x = 30
        self.adaptive_padding_y = 30

        # If in save mode
        self.output_dir = 'Photos/Cube/Match/'

        # Reference histograms
        self.ref_hists = []

        # Load and create reference histograms
        for ref_im_filename, mask_im_filename, weight in ref_im_filenames:

            # image to search with
            ref_im = cv2.imread(ref_im_filename)
            mask_im = cv2.imread(mask_im_filename)
            ref_hist = self.create_ref_hist(ref_im, mask_im, weight)
            self.ref_hists.append(ref_hist)

    # Do weighted backprojection thresholding
    def create_cube_threshold(self, query_im):

        query_hsv = cv2.cvtColor(query_im, cv2.COLOR_BGR2HSV)

        # apply backprojections

        im_h, im_w, im_d = query_im.shape

        # Weighted threshold with multiple reference images
        master_count = np.zeros((im_h, im_w), np.uint8)

        for ref_hist, weight in self.ref_hists:

            backproj = cv2.calcBackProject([query_hsv], [0, 1], ref_hist, [0, 180, 0, 256], 1)

            # size from 7 to 11
            kernel = np.ones((9, 9), np.uint8)
            backproj = cv2.morphologyEx(backproj, cv2.MORPH_CLOSE, kernel)

            # size from 7+ should work
            kernel = np.ones((15, 15), np.uint8)
            backproj = cv2.morphologyEx(backproj, cv2.MORPH_OPEN, kernel)

            kernel = np.ones((15, 15), np.uint8)
            backproj = cv2.morphologyEx(backproj, cv2.MORPH_CLOSE, kernel)

            _, threshold = cv2.threshold(backproj, 80, 255, cv2.THRESH_BINARY)

            master_count = master_count + (threshold * weight).astype(np.uint8)

        # Combine all backprojections
        _, master_thresh = cv2.threshold(master_count, 45, 255, cv2.THRESH_BINARY)

        return master_thresh

    # Threshold by green coloring
    def color_threshold(self, query_im):

        im_h, im_w, _ = query_im.shape

        query_hsv = cv2.cvtColor(query_im, cv2.COLOR_BGR2HSV)

        # Acceptable colorings
        lower_green = np.array([45, 20, 20])
        upper_green = np.array([90, 255, 255])
        green_mask = cv2.inRange(query_hsv, lower_green, upper_green)

        # Clean up noise
        kernel = np.ones((3, 3), np.uint8)
        green_mask = cv2.morphologyEx(green_mask, cv2.MORPH_ERODE, kernel)

        kernel = np.ones((5, 5), np.uint8)
        green_mask = cv2.morphologyEx(green_mask, cv2.MORPH_DILATE, kernel)

        green_thresh = cv2.bitwise_and(query_hsv, query_hsv, mask=green_mask)
        green_thresh = cv2.cvtColor(green_thresh, cv2.COLOR_HSV2BGR)

        # Find blobs!
        contours, _ = cv2.findContours(green_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        bounding_rectangle = None

        for contour in contours:

            # Approximate bounding boxes
            poly = cv2.approxPolyDP(contour, 3, True)
            x, y, w, h = cv2.boundingRect(poly)
            ar = float(w)/float(h)

            # Make sure rectangle big enough and in lower 2/3 of image
            if w*h > self.size_threshold and y > im_h/3 and \
                    self.aspect_ratio_ub >= ar >= self.aspect_ratio_lb:

                # Take the biggest rectangle so far!
                # And better aspect ratio score

                if bounding_rectangle is None:
                    bounding_rectangle = (x, y, w, h)
                else:
                    _, _, old_w, old_h = bounding_rectangle

                    ar_score = self.aspect_ratio_score(w, h)
                    old_ar_score = self.aspect_ratio_score(old_w, old_h)

                    if w*h > old_w*old_h and ar_score >= old_ar_score:
                        bounding_rectangle = (x, y, w, h)

        if bounding_rectangle is not None:

            x, y, w, h = bounding_rectangle
            cv2.rectangle(green_thresh, (x, y), (x + w, y + h), (255, 0, 0), thickness=3)

        return green_thresh, bounding_rectangle

    # Kickoff detection
    def detect(self, query_im=None, query_im_filename=None, write_out=False):

        # Check if in realtime mode or test mode
        if query_im_filename is not None:
            # image to query
            query_im = cv2.imread(query_im_filename)

        if query_im is None:
            raise Exception('Both inputs were None')

        # choose better box

        green_thresh, color_rect = self.color_threshold(query_im)

        if color_rect is not None:
            x, y, w, h = color_rect
            cv2.rectangle(query_im, (x, y), (x+w, y+h), (255, 0, 200), thickness=2)

        bounding_rectangle = color_rect

        if write_out:
            out = query_im
            fn = os.path.basename(query_im_filename)
            cv2.imwrite(self.output_dir + fn, out)

        # esimate distance
        h, w, _ = query_im.shape
        estimated_distance = self.estimate_distance((w, h), bounding_rectangle)

        return bounding_rectangle, estimated_distance
        # check if bounding box big enough


if __name__ == "__main__":

    # if running standalone

    # The tuple contains the reference image, the mask image, and the weight to assign
    cube_refs = [
        ('Photos/Cube/4.png', 'Photos/Cube/4-mask.png', 0.2),
        ('Photos/Cube/10.png', 'Photos/Cube/10-mask.png', 0.2),
        ('Photos/Cube/12.png', 'Photos/Cube/12-mask.png', 0.6)
    ]
    cd = CubeDetector(cube_refs)
    cd.output_dir = 'Photos/Cube2/Match/'

    for i in range(82):

        filename = 'Photos/Cube2/' + str(i) + '.png'
        _, _ = cd.detect(query_im_filename=filename)
