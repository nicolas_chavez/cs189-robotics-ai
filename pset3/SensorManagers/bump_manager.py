
import rospy

from kobuki_msgs.msg import BumperEvent

# Handles incoming bump sensor data, callbacks


class BumpManager:

    def __init__(self):

        self.bump_dir = None

    def start(self):

        rospy.Subscriber('mobile_base/events/bumper', BumperEvent, self.on_bump)

    # Process bump events
    def on_bump(self, data):

        if data.state == BumperEvent.PRESSED:
            self.bump_dir = data.bumper - 1
        else:
            self.bump_dir = None

    # True if have bumped
    def have_bumped(self):

        return self.bump_dir is not None
