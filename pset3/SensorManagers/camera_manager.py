
import rospy
import numpy as np
import cv2

from threading import Thread

from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

from ObjectDetectors.bottle_detector import BottleDetector
from ObjectDetectors.cone_detector import ConeDetector
from ObjectDetectors.cube_detector import CubeDetector

# Handles cleaning up camera images and controlling vision modules


class CameraManager:

    # Identifiers for object
    BOTTLE_OBJ = 0
    CONE_OBJ = 1
    CUBE_OBJ = 2

    def __init__(self):

        # Reference image for vision back projection modules

        bottle_refs = [
            ('Photos/Bottle2/53.png', 'Photos/Bottle2/53-mask2.png', 0.1),
            ('Photos/Bottle2/54.png', 'Photos/Bottle2/54-mask2.png', 0.1),
            ('Photos/Bottle2/55.png', 'Photos/Bottle2/55-mask2.png', 0.1),
            ('Photos/Bottle2/56.png', 'Photos/Bottle2/56-mask2.png', 0.1),
            ('Photos/Bottle2/57.png', 'Photos/Bottle2/57-mask2.png', 0.1),
            ('Photos/Bottle2/58.png', 'Photos/Bottle2/58-mask2.png', 0.1),
            ('Photos/Bottle2/59.png', 'Photos/Bottle2/59-mask2.png', 0.1),
            ('Photos/Bottle2/60.png', 'Photos/Bottle2/60-mask2.png', 0.1),
            ('Photos/Bottle2/61.png', 'Photos/Bottle2/61-mask2.png', 0.1),
            ('Photos/Bottle2/0.png', 'Photos/Bottle2/0-mask2.png', 0.1),
        ]
        cone_refs = [
            ('Photos/Cone/43.png', 'Photos/Cone/43-mask.png', 0.5),
            ('Photos/Cone/11.png', 'Photos/Cone/11-mask.png', 0.2),
            ('Photos/Cone/14.png', 'Photos/Cone/14-mask.png', 0.2),
            ('Photos/Cone/22.png', 'Photos/Cone/22-mask.png', 0.05),
            ('Photos/Cone/58.png', 'Photos/Cone/58-mask.png', 0.05)
        ]
        cube_refs = [
            ('Photos/Cube/4.png', 'Photos/Cube/4-mask.png', 0.2),
            ('Photos/Cube/10.png', 'Photos/Cube/10-mask.png', 0.2),
            ('Photos/Cube/12.png', 'Photos/Cube/12-mask.png', 0.6)
        ]

        self.bottle_detector = BottleDetector(bottle_refs)
        self.cone_detector = ConeDetector(cone_refs)
        self.cube_detector = CubeDetector(cube_refs)

        self.started = False
        # Keeps track if busy working to analyze image
        self.busy_processing = False

        self.on_detect_callback = None  # on_detect(bounding_rect, est_distance, obj_kind)

        # callback tracker across threads
        self.detect_tracker = {CameraManager.BOTTLE_OBJ: None, CameraManager.CONE_OBJ: None, CameraManager.CUBE_OBJ: None}

        self.cv_bridge = CvBridge()

        # If still interested in detecting certain objects
        self.to_detect = [True, True, True]

    def start(self):

        self.started = True
        rospy.Subscriber('/camera/rgb/image_raw', Image, self.on_camera_image, queue_size=1,  buff_size=2**24)

    # Does the heavy lifting
    def process_image(self, image):

        bottle_rect = None
        cone_rect = None
        cube_rect = None

        # If still interested in detecting, then try to do so
        if self.to_detect[CameraManager.BOTTLE_OBJ]:
            bottle_rect, bottle_dist = self.bottle_detector.detect(image)

        if self.to_detect[CameraManager.CONE_OBJ]:
            cone_rect, cone_dist = self.cone_detector.detect(image)

        if self.to_detect[CameraManager.CUBE_OBJ]:
            cube_rect, cube_dist = self.cube_detector.detect(image)

        # Save these results to be called on main thread

        if bottle_rect is not None:
            self.store_detect_data(bottle_rect, bottle_dist, CameraManager.BOTTLE_OBJ)

        if cone_rect is not None:
            self.store_detect_data(cone_rect, cone_dist, CameraManager.CONE_OBJ)

        if cube_rect is not None:
            self.store_detect_data(cube_rect, cube_dist, CameraManager.CUBE_OBJ)

        self.busy_processing = False

    # Save detection data
    def store_detect_data(self, bounding_rect, est_distance, obj_kind):

        self.detect_tracker[obj_kind] = (bounding_rect, est_distance)

    # Debug the dectections by drawing them
    def draw_detections(self, image):

        colors = [(0, 255, 0), (255, 0, 0), (0, 0, 255)]

        # BOTTLE GREEN
        # CONE BLUE
        # CUBE RED

        obj_kinds = self.detect_tracker.keys()

        for i in range(len(obj_kinds)):

            obj_kind = obj_kinds[i]

            track_data = self.detect_tracker[obj_kind]

            if track_data is not None:
                r, _ = track_data
                x, y, w, h = r

                cv2.rectangle(image, (x, y), (x+w, y+h), colors[i], thickness=3)

    # Ckears detection data for next update and informs listeners
    def flush_tracker(self):

        if self.on_detect_callback is not None:

            for i in range(2, -1, -1):

                obj_kind = i

                track_data = self.detect_tracker[obj_kind]
                if track_data is not None:
                    r, e = track_data
                    self.detect_tracker[obj_kind] = None
                    self.on_detect_callback(r, e, obj_kind)

    # Called when image from CV is received
    def on_camera_image(self, data):

        if not self.started:
            return

        image = self.cv_bridge.imgmsg_to_cv2(data, 'bgr8')

        # Crop the same way depth images are cropped
        cropping_rect = (50, 120, 540, 340)
        crop_x, crop_y, crop_w, crop_h = cropping_rect
        image = image[crop_y:crop_y+crop_h, crop_x:crop_x+crop_w]

        if image is not None:

            display_image = np.copy(image)

            self.draw_detections(display_image)

            cv2.imshow('Color', display_image)
            cv2.waitKey(3)

            # Flush current processed data
            self.flush_tracker()

            # Analyze, if possible
            if not self.busy_processing:

                self.busy_processing = True
                self.process_image(image)
                # Start thread
                # self.busy_processing = True
                # processing_thread = Thread(group=None, target=self.process_image, args=[image])
                # processing_thread.daemon = True
                # processing_thread.start()

