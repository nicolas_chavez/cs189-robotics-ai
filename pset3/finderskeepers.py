import rospy

import time

from OutputManagers.sound_manager import SoundManager
from OutputManagers.motion_manager import MotionManager

from SensorManagers.bump_manager import BumpManager
from SensorManagers.cliff_manager import CliffManager
from SensorManagers.depth_manager import DepthManager
from SensorManagers.wheel_drop_manager import WheelDropManager
from SensorManagers.camera_manager import CameraManager

from Brainz.navigation_lobe import NavigationLobe


# The root object that operates at the highest level

class FinderKeeper:

    # Time to wait since last callbacks to return to default states
    BOX_OUT_TIMEOUT_MS = 1500
    OBJ_DETECT_TIMEOUT_MS = 500

    def __init__(self):

        self.is_shutdown = False

        # Boxed out means the robot is trapped by the obstacles in front of it
        self.is_boxed_out = False
        # The direction with the most obstacles
        self.box_out_dir = None
        # The last time since it was boxed out, keep track of to return to normal state if not continued
        self.last_box_out_time = 0

        # The last time an object was detected, keep track of to return to non homing state
        self.last_obj_detect_time = 0

        # Instantiate different managers

        self.motion = MotionManager()
        self.sound = SoundManager()

        self.camera = CameraManager()
        self.bump = BumpManager()
        self.cliff = CliffManager()
        self.depth = DepthManager()
        self.wheel_drop = WheelDropManager()

        self.navigation = NavigationLobe()

        ########

        # The current target's bounding box, its estimated distance, and the kidn of object it is
        self.current_target = None, None, None

        # If an object was just claimed in last cycle
        self.just_claimed = False

        # Keep track of which objects have been found or not
        self.claimed_objects = [False, False, False]

    # Main loop
    def run(self):

        # Initialization

        rospy.init_node('FindersKeepers', anonymous=False)
        rospy.on_shutdown(self.shutdown)

        # Start modules

        self.motion.start()
        self.sound.start()

        self.camera.start()
        self.bump.start()
        self.cliff.start()
        self.depth.start()
        self.wheel_drop.start()

        self.navigation.start()

        # rotate away if cliffed
        self.cliff.on_cliff_callback = self.on_cliff

        # shutdown if wheel dropped
        self.wheel_drop.on_drop_callback = self.shutdown

        # callback for object detection
        self.camera.on_detect_callback = self.on_object_detected

        # callback for depth information
        self.depth.on_process_callback = self.on_depth_processed

        # callback for boxed out
        self.navigation.boxed_out_callback = self.on_box_out

        r = rospy.Rate(10)

        # as long as you haven't shutdown keeping doing...
        while not self.is_shutdown:

            if self.just_claimed:
                # If object was just claimed, back up
                self.motion.move_backward()
                self.just_claimed = False

            # If there is a bump, back up and turn in the appropriate direction
            elif self.bump.have_bumped():
                # Back it up, but only a tiny bit
                bump_dir = self.bump.bump_dir
                self.motion.move_backward()
                self.motion.rotate_away(bump_dir)

            elif self.is_boxed_out:
                # We're trapped, so rotate until not case anymore
                self.motion.rotate_tiny_bit(self.box_out_dir)

            else:
                # Move forward otherwise
                best_horizontal = self.navigation.route_decision()
                ang_vel = -1 * best_horizontal
                # ang_vel = (-0.5 * np.sign(best_horizontal) * abs(best_horizontal**0.5))
                self.motion.move_forward(lin_vel=0.3, ang_vel=ang_vel)

            # Keep track of time since being boxed out and homing
            cur_millis = int(round(time.time() * 1000))

            # Not boxed out anymore
            if cur_millis - self.last_box_out_time > FinderKeeper.BOX_OUT_TIMEOUT_MS:
                self.is_boxed_out = False
                self.box_out_dir = None

            # Not homing on target anymore
            if cur_millis - self.last_obj_detect_time > FinderKeeper.OBJ_DETECT_TIMEOUT_MS:
                self.current_target = None, None, None

            # wait for 0.1 seconds (10 HZ) and publish again
            r.sleep()

    # Take action on an object being detected
    def on_object_detected(self, bounding_rectangle, est_distance, obj_kind):

        # Update tracking data, but only if not claimed yet

        if (obj_kind is CameraManager.BOTTLE_OBJ and not self.claimed_objects[CameraManager.BOTTLE_OBJ]) or \
           (obj_kind is CameraManager.CONE_OBJ and not self.claimed_objects[CameraManager.CONE_OBJ]) or \
           (obj_kind is CameraManager.CUBE_OBJ and not self.claimed_objects[CameraManager.CUBE_OBJ]):

            self.current_target = (bounding_rectangle, est_distance, obj_kind)
            self.last_obj_detect_time = int(round(time.time() * 1000))

    # Take action when cliff sensor goes off
    def on_cliff(self):

        # Back up and turn away
        self.motion.move_backward()
        self.motion.rotate_slow()

    # Take action when faced with too many obstacles
    def on_box_out(self, boxed_dir):

        # Set the trigger and the timeout
        self.is_boxed_out = True

        cur_millis = int(round(time.time() * 1000))
        if cur_millis - self.last_box_out_time > FinderKeeper.BOX_OUT_TIMEOUT_MS:

            self.box_out_dir = boxed_dir

        self.last_box_out_time = cur_millis

    # Handle depth info
    def on_depth_processed(self, image):

        target_bounding_box, target_distance, target_kind = self.current_target

        # Feed depth info to navigation

        _, _, found_obj = self.navigation.route_decide(image, target_bounding_box, target_distance, target_kind)

        # If an object was found, claim it
        if found_obj:
            self.claim_object(target_kind)

    # Claims a target
    def claim_object(self, target_kind):

        # Remember what has been claimed, and don't detect it anymore
        self.claimed_objects[target_kind] = True
        self.camera.to_detect[target_kind] = False

        self.sound.make_beep()
        print "Found %d " % target_kind

        self.current_target = None, None, None

        # Check if all objects have been found

        found_all = True
        for found_obj in self.claimed_objects:
            found_all = found_all and found_obj

        if found_all:
            self.declare_victory()

        # Set trigger
        self.just_claimed = True

    # Win!
    def declare_victory(self):

        self.motion.started = False

        # Sing for the audience
        self.sound.make_beep()
        rospy.sleep(1)
        self.sound.make_beep()
        rospy.sleep(1)
        self.sound.make_beep()
        rospy.sleep(1)
        self.sound.make_beep()
        rospy.sleep(1)
        self.sound.make_beep()
        rospy.sleep(1)
        self.sound.make_beep()
        rospy.sleep(1)
        self.sound.make_beep()
        rospy.sleep(1)
        self.sound.make_beep()
        rospy.sleep(1)
        self.sound.make_beep()
        rospy.sleep(1)

        self.shutdown()

    # Stop all activities
    def shutdown(self):

        # stop turtlebot
        self.is_shutdown = True
        rospy.loginfo("Stop TurtleBot")
        self.motion.stop_moving()
        # sleep just makes sure TurtleBot receives the stop command prior to shutting down the script
        rospy.sleep(5)


if __name__ == "__main__":

    fk = FinderKeeper()
    fk.run()
