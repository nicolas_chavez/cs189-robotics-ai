
import cv2
import numpy as np

from object_detector import ObjectDetector


class BandDetector(ObjectDetector):

    """

    Object detection module handles recognizing the band in a color image scene. It finds all bright pink
    splotches and green splotches and sees where they are close enough in a reasonable size, location and
    orientation to determine the band.

    """

    def __init__(self):

        ObjectDetector.__init__(self)

        self.size_threshold = 100  # 450
        self.aspect_ratio_lb = 0.1
        self.aspect_ratio_ideal = 1
        self.aspect_ratio_ub = float("inf")
        self.val_threshold = 0  # 50

        self.adaptive_padding_x = 45
        self.adaptive_padding_y = 65

    # Threshold by green coloring
    @staticmethod
    def color_threshold(query_im):

        """

        Finds all bright pink splotches and green splotches and sees where they are close enough to
        produce a threshold where a band may be present.

        :param query_im: Color image to be queried.
        :return: Binary image thresholded to where band may be present.
        """

        im_h, im_w, _ = query_im.shape

        query_hsv = cv2.cvtColor(query_im, cv2.COLOR_BGR2HSV)

        # Acceptable colorings
        lower_green = np.array([33, 40, 30])
        upper_green = np.array([90, 255, 240])

        lower_pink = np.array([150, 100, 50])
        upper_pink = np.array([180, 255, 255])

        green_mask = cv2.inRange(query_hsv, lower_green, upper_green)
        pink_mask = cv2.inRange(query_hsv, lower_pink, upper_pink)

        # Merge the two color masks and expand to find 'closeness' of the two

        kernel = np.ones((5, 5), np.uint8)
        green_mask = cv2.morphologyEx(green_mask, cv2.MORPH_DILATE, kernel)
        kernel = np.ones((7, 7), np.uint8)
        green_mask = cv2.morphologyEx(green_mask, cv2.MORPH_ERODE, kernel)
        kernel = np.ones((19, 19), np.uint8)
        green_super_mask = cv2.morphologyEx(green_mask, cv2.MORPH_DILATE, kernel)

        kernel = np.ones((5, 5), np.uint8)
        pink_mask = cv2.morphologyEx(pink_mask, cv2.MORPH_DILATE, kernel)
        kernel = np.ones((7, 7), np.uint8)
        pink_mask = cv2.morphologyEx(pink_mask, cv2.MORPH_ERODE, kernel)
        kernel = np.ones((19, 19), np.uint8)
        pink_super_mask = cv2.morphologyEx(pink_mask, cv2.MORPH_DILATE, kernel)

        band_sub_mask = cv2.bitwise_or(pink_mask, green_mask)
        band_super_mask = cv2.bitwise_and(pink_super_mask, green_super_mask)

        # Clean up noise

        band_mask = cv2.bitwise_and(band_super_mask, band_sub_mask)

        kernel = np.ones((7, 7), np.uint8)
        band_mask = cv2.morphologyEx(band_mask, cv2.MORPH_DILATE, kernel)

        band_thresh = cv2.bitwise_and(query_hsv, query_hsv, mask=band_mask)
        band_thresh = cv2.cvtColor(band_thresh, cv2.COLOR_HSV2BGR)

        band_super_thresh = cv2.bitwise_and(query_hsv, query_hsv, mask=band_super_mask)
        band_super_thresh = cv2.cvtColor(band_super_thresh, cv2.COLOR_HSV2BGR)

        band_sub_thresh = cv2.bitwise_and(query_hsv, query_hsv, mask=band_sub_mask)
        band_sub_thresh = cv2.cvtColor(band_sub_thresh, cv2.COLOR_HSV2BGR)

        return band_mask

    def rect_distance(self, rect1, rect2):

        """

        Calculates a 'closeness' of two rectangles based on size and position.

        :param rect1:
        :param rect2:
        :return: A 'rectangle distance' score
        """

        x1, y1, w1, h1 = rect1
        x2, y2, w2, h2 = rect2

        cx_1 = x1 + (w1/2)
        cy_1 = y1 + (h1/2)
        cx_2 = x2 + (w2/2)
        cy_2 = y2 + (h2/2)

        center_dist = (cx_2 - cx_1)**2 + (cy_2 - cy_1)**2
        center_dist **= 0.5

        area1 = w1*h1
        area2 = w2*h2
        # Gets geometric difference and then a number from 0+ that is better when lower
        area_diff = float(area1)/area2
        area_diff = abs(1.0 - area_diff)

        max_score = 500

        if center_dist == 0:
            center_score = max_score
        else:
            center_score = min(max_score, float(1)/center_dist)

        if area_diff == 0:
            area_score = max_score
        else:
            area_score = min(max_score, float(1)/area_diff)

        score = (center_score**2) + 0.2*(area_score**2)
        score **= 0.5

        return score

    @staticmethod
    def calculate_distance(depth_image, target_rect):

        """

        Tries to estimate how far target is with its bounding box and depth image.

        :param depth_image: Depth image of the camera.
        :param target_rect: Bounding box of target
        :return: Distance in meters to target
        """

        # Takes mean of region of interest in depth camera

        if depth_image is None:
            return 2

        x, y, w, h = target_rect
        roi = depth_image[y:y+h, x:x+w, 0]

        # find like the mean depth in this box
        mean_depth = np.nanmean(np.nanmean(roi))

        return mean_depth

    def detect(self, color_image, depth_image, last_target):

        """

        Detects the band in the image.

        :param color_image: The color information for vision detection.
        :param depth_image: Used to determine how far the band is from camera.
        :param last_target: The last location the band was found in, to improve tracking.
        :return: (bounding box, distance)
        """

        im_h, im_w, _ = color_image.shape

        # Do fancy color thresholding for band colors.
        band_mask = self.color_threshold(color_image)

        # Find blobs!
        contours, _ = cv2.findContours(band_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cv2.drawContours(color_image, contours, -1, (200, 0, 200), thickness=2)
        bounding_rectangle = None

        # Find bounding boxes, vett them
        for contour in contours:

            # Approximate bounding boxes
            poly = cv2.approxPolyDP(contour, 3, True)
            x, y, w, h = cv2.boundingRect(poly)
            ar = float(w)/float(h)

            # Make sure rectangle big enough and in lower part of image
            if w*h > self.size_threshold and y > (im_h * 0.25) and \
                    self.aspect_ratio_ub >= ar >= self.aspect_ratio_lb:

                # Take the biggest rectangle so far!
                # And better aspect ratio score

                if bounding_rectangle is None:
                    bounding_rectangle = (x, y, w, h)
                else:
                    last_rect, _ = last_target
                    if last_rect:
                        old_dist_score = self.rect_distance(bounding_rectangle, last_rect)
                        cur_dist_score = self.rect_distance((x, y, w, h), last_rect)

                        if cur_dist_score >= old_dist_score:
                            bounding_rectangle = (x, y, w, h)
                    else:
                        bounding_rectangle = (x, y, w, h)

        # Use depth info to calculate distance to band
        if bounding_rectangle:
            distance = self.calculate_distance(depth_image, bounding_rectangle)
            print distance
            target = (bounding_rectangle, distance)
        else:
            target = None, None

        return target
