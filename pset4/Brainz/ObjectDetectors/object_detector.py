
import cv2
import numpy as np

# Superclass for detector classes, provides common methods used by all


class ObjectDetector:

    def __init__(self):

        # Used to determine how well bounding box matches the desired aspect ratio
        # Used in self.aspect_ratio_score to determine how well of a fit
        self.aspect_ratio_lb = 0
        self.aspect_ratio_ideal = 1
        self.aspect_ratio_ub = float("inf")
        self.size_threshold = 0

        # How many more border pixels to consider when doing adaptive thresholding
        self.adaptive_padding_x = 0
        self.adaptive_padding_y = 0

    # Given the width and height of a target bounding box, how well does it it desired aspect ratio
    # Bigger number is better
    def aspect_ratio_score(self, w, h):

        # distance from aspect ratio to bound's midpoint
        ar = float(w)/float(h)

        mid_ar = (self.aspect_ratio_ub - self.aspect_ratio_lb)/2 + self.aspect_ratio_lb

        if mid_ar == ar:
            score = float("inf")
        else:
            score = float(1) / abs(mid_ar - ar)

        if ar == self.aspect_ratio_ideal:
            ideal_dist = float("inf")
        else:
            ideal_dist = float(1) / abs(self.aspect_ratio_ideal - ar)

        score += ideal_dist

        # bigger score is better
        return score

    # Creates the model/reference histogram to be used during back projection
    # Takes in reference image, the mask to apply to it
    def create_ref_hist(self, ref_im, mask_im, weight):

        # Gaussian blur?

        ref_hsv = cv2.cvtColor(ref_im, cv2.COLOR_BGR2HSV)
        # 180 hue values, 256 saturation values
        # [0, 1] indicates hue and saturation channels only
        mask_gray = cv2.cvtColor(mask_im, cv2.COLOR_BGR2GRAY)
        _, ref_mask = cv2.threshold(mask_gray, 0, 255, cv2.THRESH_BINARY)

        ref_hist = cv2.calcHist([ref_hsv], [0, 1], ref_mask, [180, 256], [0, 180, 0, 256])
        cv2.normalize(ref_hist, ref_hist, 0, 255, cv2.NORM_MINMAX)

        return ref_hist, weight

    # Querys the threshold data for the best bounding box, if any
    def find_rect_in_threshold(self, query_im, threshold):

        # Convert to hsv
        query_hsv = cv2.cvtColor(query_im, cv2.COLOR_BGR2HSV)
        im_h, im_w = threshold.shape

        # Find the blobs!
        contours, _ = cv2.findContours(threshold, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        bounding_rectangle = None
        for contour in contours:

            # Approximate a bounding box for each blob
            poly = cv2.approxPolyDP(contour, 3, True)
            x, y, w, h = cv2.boundingRect(poly)
            ar = float(w)/float(h)

            # Find mean value of roi
            roi_v = query_hsv[y:y+h, x:x+w, 2]
            mean_val = cv2.mean(roi_v)[0]

            # Make sure rectangle big enough and in lower 2/3 of image
            if w*h > self.size_threshold and y > im_h/3 and self.aspect_ratio_ub >= ar >= self.aspect_ratio_lb and mean_val >= self.val_threshold:

                # Take the biggest rectangle so far!
                # And better aspect ratio score

                if bounding_rectangle is None:
                    bounding_rectangle = (x, y, w, h)
                else:
                    _, _, old_w, old_h = bounding_rectangle

                    ar_score = self.aspect_ratio_score(w, h)
                    old_ar_score = self.aspect_ratio_score(old_w, old_h)

                    if w*h > old_w*old_h and ar_score >= old_ar_score:
                        bounding_rectangle = (x, y, w, h)

        return bounding_rectangle, contours

    # Performs an adaptive threshold (to better mask) and creates another bounding box
    def adaptive_threshold_pass(self, query_im, bounding_rectangle):

        if bounding_rectangle is None:
            return None

        im_h, im_w, _ = query_im.shape
        query_hsv = cv2.cvtColor(query_im, cv2.COLOR_BGR2HSV)

        # Add padding to bounding box, which is usually underfitting
        x, y, w, h = bounding_rectangle
        buffer_px_size_x = self.adaptive_padding_x
        buffer_px_size_y = self.adaptive_padding_y

        # enlarge window
        x -= buffer_px_size_x
        y -= buffer_px_size_y
        ex = x + w + buffer_px_size_x * 2
        ey = y + h + buffer_px_size_y * 2

        x = max(0, x)
        y = max(0, y)
        ex = min(im_w, ex)
        ey = min(im_h, ey)

        w = ex - x
        h = ey - y

        # select roi
        adaptive_thresh_target = query_hsv[y:y+h, x:x+w, 0]
        adaptive_thresh_target = adaptive_thresh_target.astype(np.uint8)

        adaptive_thresh = cv2.adaptiveThreshold(adaptive_thresh_target, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 25, 0)

        # insert into empty image of previous original size
        fill = np.zeros((im_h, im_w), np.uint8)

        fill[y:y+h, x:x+w] = adaptive_thresh

        adaptive_thresh = fill

        # Clean up some noise

        kernel = np.ones((3, 3), np.uint8)
        adaptive_thresh = cv2.morphologyEx(adaptive_thresh, cv2.MORPH_ERODE, kernel)

        kernel = np.ones((7, 7), np.uint8)
        adaptive_thresh = cv2.morphologyEx(adaptive_thresh, cv2.MORPH_DILATE, kernel)

        kernel = np.ones((13, 13), np.uint8)
        adaptive_thresh = cv2.morphologyEx(adaptive_thresh, cv2.MORPH_ERODE, kernel)

        kernel = np.ones((13, 13), np.uint8)
        adaptive_thresh = cv2.morphologyEx(adaptive_thresh, cv2.MORPH_DILATE, kernel)

        contours, _ = cv2.findContours(adaptive_thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        # Check blobs
        adaptive_rectangle = None
        for contour in contours:

            # Approximate bounding boxes
            poly = cv2.approxPolyDP(contour, 3, True)
            c_x, c_y, c_w, c_h = cv2.boundingRect(poly)

            # Take the biggest rectangle so far!
            # And better aspect ratio

            if adaptive_rectangle is None:
                adaptive_rectangle = (c_x, c_y, c_w, c_h)
            else:
                _, _, old_w, old_h = adaptive_rectangle

                if c_w*c_h > old_w*old_h:
                    adaptive_rectangle = (c_x, c_y, c_w, c_h)

        return adaptive_rectangle

    # Given bounding box, estimate the distance of object
    def estimate_distance(self, image_size, bounding_rectangle):

        if bounding_rectangle is None:
            return None

        r_x, r_y, r_w, r_h = bounding_rectangle

        # Very simple for now
        return float(1)/float(r_w*r_h)

    # Stub method for doing detection, subclasses should override
    def detect(self, query_im=None, query_im_filename=None, write_out=False):

        bounding_rectangle = None
        distance_estimate = None

        return bounding_rectangle, distance_estimate
