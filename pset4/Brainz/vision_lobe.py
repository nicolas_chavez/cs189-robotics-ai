
import cv2
import numpy as np
import time

from ObjectDetectors.band_detector import BandDetector
from ObjectDetectors.threshold_gui import ThresholdGUI


class VisionLobe:

    """

    Just manages receiving the color image from the camera. Draws some debug info too.

    """

    def __init__(self):

        self.started = False

        self.band_detector = BandDetector()

        self.thresh_gui = ThresholdGUI()

    def start(self):

        self.started = True

    def track(self, color_image, depth_image, last_target):

        target = self.band_detector.detect(color_image, depth_image, last_target)

        disp_image = np.copy(color_image)

        t_rect, t_dist = target
        if t_rect:
            x, y, w, h = t_rect
            cv2.rectangle(disp_image, (x, y), (x+w, y+h), (255, 0, 0), thickness=2)

        cv2.imshow('Color', disp_image)
        cv2.waitKey(3)

        return target

if __name__ == "__main__":

    vl = VisionLobe()

    vl.start()

    last_t = None

    # for i in range(1232):
    for i in range(834):
        image_fn = '../../../TrackData/images-band/images-1/picture-' + str(i) + '.png'
        color_im = cv2.imread(image_fn)
        last_t = vl.track(color_im, None, last_t)
        # time.sleep(0.1)
