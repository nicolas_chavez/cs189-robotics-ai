File tree:

Brainz
	+-> ObjectDetectors
		+-> band_detector.py: functions related to the detection of the band from the RGB color image
		+-> object_detector.py: definition of objectDetector class which allows for us to easily create a number of
		    objects to be recognized and aids in ascribing general functions
		+-> threshold_gui.py: helper application to be able to determine color parameters for detector calibration
	+-> navigation_lobe.py: handles navigation related decisions including returning parameters for motion paths based
	    off of depth image information and information related to the location of the detected objects
	+-> vision_lobe.py:  does some pre-processing and passes RGB feed to object_detector

OutputManagers
	+-> motion_manager.py: handles instructions related to object motion including path commands generated in
	    navigation_lobe or pre-determined paths such as response when bumped
	+-> sound_manager.py: handles instructions related to beeps

SensorManagers
	+-> bump_manager.py: handles lowest level code related to procuring information from the bump sensor
	+-> camera_manager.py: handles lowest level code related to procuring an RGB feed from the 3dsensor
	+-> cliff_manager.py: handles lowest level code related to procuring information from the cliff sensor
	+-> depth_manager.py: handles lowest level code related to procuring information from the depth sensor and does
	    some depth image pre-processing
	+-> wheel_drop_manager.py: handles lowest level code related to procuring information from the wheel drop sensor

bandtracker.py: highest level code; the "main" loop. Handles passing information between lobes and managers. Structured
    in a parallel way, so while there are not specific states that robot is in, multiple objectives can be occurring and
    weighted simultaneously.



The objective for this problem set is split into the four parts:

    1) recognizing a band
    2) following at a safe distance,
    3) avoiding obstacles
    4) realizing when lost.

The first objective is achieved for the objects according to a set of weighted criteria on a color image returned by
the camera, including hue, value, object size, object aspect ratio. Our band is a striped green and pink band making
it's color unique amongst the objects the robot sees. In addition to detecting the pink and green in the image, the
color swatches must be co-located and have the right aspect ratio, size, and general location within the image. The
number of criteria we have makes the robot not chase false positives. See the Vision Lobe module (VisionLobe.track),
and the Band Detector class (BandDetector.detect). The main class in this case is now the BandTracker class.

In addition to returning a bounding box for the band, we also have functions that pull the depth of the band location
from the depth image. For objective #2, this allows the robot to scale its speed proportional to band distance, so it
does not fall too far behind the person and does not approach too close. Furthermore the location of the bounding box
tells the robot what direction is must home into to approach the band. See (BandDetector.calculate_distance)

This information is not fed directly to the robots wheels, instead, it is weighted in a multi-faceted cost function with
information about other object locations in the depth image (NavigationLobe.route_decide) such that the
robot will continue to avoid obstacle while in pursuit of the band. Therefore paths to the band may not be the most
direct routes but one that keeps the robot "most safe". It will stay in open spaces as it homes in on the band,
as per objective 3. If, for instance, the person walks through a space that is too tight for the robot, such that the
robot will not be able to pass through (however can still see the band), the robot will stop, go into a scanning mode
and beep to indicate that it cannot follow. (BandTracker.is_boxed_out)

For objective #4, if the robot has lost the user for a number of frames one of two things will happen. If the the number
of frames is minimal the robot will continue to pursue the band in it's last know location. If the number of frames in
large, the robot assumes it has lost you around a corner. The robot will move forward (while maintaining obstacle
avoidance) to the distance where if last saw the band and the go into scanning mode. In this mode the robot turns to
the direction it saw the band last and rotates slowly while beeping (latter half of BandTracker.run).