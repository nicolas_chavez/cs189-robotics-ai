
import rospy

from kobuki_msgs.msg import CliffEvent


class CliffManager:

    """

    Handles inputs from cliff sensor, calls back into band tracker to inform emergency.

    """

    def __init__(self):

        self.cliff_dir = None
        self.on_cliff_callback = None  # ()

    def start(self, callback=None):

        self.on_cliff_callback = callback
        rospy.Subscriber('mobile_base/events/cliff', CliffEvent, self.on_cliff)

    # Process bump events
    def on_cliff(self, data):

        if data.state == CliffEvent.CLIFF:
            # save the cliff sensor direction
            self.cliff_dir = data.sensor - 1

            if self.on_cliff_callback is not None:
                self.on_cliff_callback()

        else:
            # cliff is not true anymore
            self.cliff_dir = None

    # True if have off cliff
    def have_cliffed(self):

        return self.cliff_dir is not None
