
import time
import math
import cv2
import numpy as np
import rospy

from sensor_msgs.msg import PointCloud2, LaserScan, Image
from cv_bridge import CvBridge, CvBridgeError


class DepthManager:

    """

    Handles receiving and cleaning up depth data to pass onto vision lobe eventually.
    Doesn't do too much except crop and abstract ros subscriptions.

    """

    def __init__(self):

        self.current_image = None

        self.started = False

        # callback for depth info processed
        self.on_image_callback = None  # (depth_image)

        self.bridge = CvBridge()

    def start(self):

        self.started = True
        rospy.Subscriber('/camera/depth/image', Image, self.on_depth_image, queue_size=1, buff_size=2 ** 24)

    def on_depth_image(self, data):

        if not self.started:
            return

        # Use bridge to convert to CV::Mat type.
        # NOTE: SOME DATA WILL BE 'NaN'
        # and numbers correspond to distance to camera in meters
        cv_image = self.bridge.imgmsg_to_cv2(data, 'passthrough')

        # manipulate the image directly here!
        # Crop the image to remove irrelevant info (bars in front of robot)
        cropping_rect = (50, 120, 540, 340)
        crop_x, crop_y, crop_w, crop_h = cropping_rect

        cv_image_cropped = cv_image[crop_y:crop_y+crop_h, crop_x:crop_x+crop_w]

        # Blur
        cv_image_cropped = cv2.GaussianBlur(cv_image_cropped, (5, 5), 1)

        cv_image_cropped = cv2.cvtColor(cv_image_cropped, cv2.COLOR_GRAY2BGR)

        self.current_image = cv_image_cropped

        # Call listener
        if self.on_image_callback is not None:
            self.on_image_callback(self.current_image)



