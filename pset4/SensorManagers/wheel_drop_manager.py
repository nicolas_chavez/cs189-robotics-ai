
import rospy

from kobuki_msgs.msg import WheelDropEvent

# Handles receiving wheel drop info and calling back


class WheelDropManager:

    def __init__(self):

        self.started = False
        self.is_wheel_drop = False
        self.on_drop_callback = None  # ()

    def start(self, callback=None):

        self.started = True
        self.on_drop_callback = callback
        # Subscribe to receive wheel drop data; process with processCliffSensing
        rospy.Subscriber('mobile_base/events/wheel_drop', WheelDropEvent, self.on_wheel_drop)

    # Called when wheel drop change detected
    def on_wheel_drop(self, data):

        self.is_wheel_drop = (data.state == WheelDropEvent.DROPPED)

        if self.is_wheel_drop and self.on_drop_callback is not None:

            # call listener callback
            self.on_drop_callback()
