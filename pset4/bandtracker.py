import rospy
import numpy as np
import time
import math

from OutputManagers.sound_manager import SoundManager
from OutputManagers.motion_manager import MotionManager

from SensorManagers.bump_manager import BumpManager
from SensorManagers.cliff_manager import CliffManager
from SensorManagers.depth_manager import DepthManager
from SensorManagers.wheel_drop_manager import WheelDropManager
from SensorManagers.camera_manager import CameraManager

from Brainz.navigation_lobe import NavigationLobe
from Brainz.vision_lobe import VisionLobe


class BandTracker:

    """

    Band Tracker is the main class that kicks off all sub modules/processes. It does most
    of the planning regarding planning, or deciding how to move based on what kind of info
    it receives from its vision and object avoidance modules.

    Of note are the variables 'last_band_detect_time', 'last_best_route' and 'num_frames_dropped',
    which are used when the vision modules lose a target temporarily or for an extended period.

    """

    # Time to get unfazed after the last 'boxed out' notification
    BOX_OUT_TIMEOUT_MS = 400
    # Time to try to pursue the band for after its targeting has been lost
    BAND_DETECT_TIMEOUT_MS = 1500

    def __init__(self):

        self.is_shutdown = False

        # Boxed out means the robot is trapped by the obstacles in front of it
        self.is_boxed_out = False
        # The direction with the most obstacles
        self.box_out_dir = None
        # The last time since it was boxed out, keep track of to return to normal state if not continued
        self.last_box_out_time = 0

        # The last time band was detected, keep track of to return to non homing state
        self.last_band_detect_time = 0
        # The last plan of action -- direction, speed, found -- used when target lost
        self.last_best_route = None, None, None

        # When the first frame was dropped/target lost
        self.first_frame_drop_time = 0
        # How many frames to continue trying to find lost target for
        self.frame_drop_motions_left = 0

        # How many frames so far since we lost target
        self.num_frames_dropped = 0

        # If the target has just been lost
        self.first_frame_drop = True

        # Instantiate different managers

        self.motion = MotionManager()
        self.sound = SoundManager()

        self.camera = CameraManager()
        self.bump = BumpManager()
        self.cliff = CliffManager()
        self.depth = DepthManager()
        self.wheel_drop = WheelDropManager()

        self.navigation = NavigationLobe()
        self.vision = VisionLobe()

        # The current target's bounding box, its estimated distance
        self.band_target = None, None
        # The last known target info -- bounding_box, distance
        self.last_band_target = None, None

    def run(self):

        # Initialization

        rospy.init_node('BandTracker', anonymous=False)
        rospy.on_shutdown(self.shutdown)

        # Start modules

        self.motion.start()
        self.sound.start()

        self.camera.start()
        self.bump.start()
        self.cliff.start()
        self.depth.start()
        self.wheel_drop.start()

        self.navigation.start()
        self.vision.start()

        # rotate away if cliffed
        # self.cliff.on_cliff_callback = self.on_cliff

        # shutdown if wheel dropped
        self.wheel_drop.on_drop_callback = self.shutdown

        # callback for object detection
        # self.camera.on_image_callback = self.on_object_detected

        # callback for depth information
        # self.depth.on_image_callback = self.on_depth_processed

        # callback for boxed out
        self.navigation.boxed_out_callback = self.on_box_out

        r = rospy.Rate(10)

        # as long as you haven't shutdown keeping doing...
        while not self.is_shutdown:

            # Keep track of time
            cur_millis = int(round(time.time() * 1000))

            # Whether or not trying to find target after it left frame
            still_homing = (cur_millis - self.last_band_detect_time < self.BAND_DETECT_TIMEOUT_MS)

            # Get camera data!
            color_image = self.camera.current_image
            depth_image = self.depth.current_image

            band_rect, band_dist = self.band_target

            # Track the last good target
            if band_rect is not None and not math.isnan(band_dist):
                self.last_band_target = self.band_target

            # Detect the band in the image
            if color_image is not None:
                self.band_target = self.vision.track(color_image, depth_image, self.band_target)

            band_rect, band_dist = self.band_target

            # DECISION MAKING:

            # If there is a bump, back up and turn in the appropriate direction
            if self.bump.have_bumped():
                # Back it up, but only a tiny bit
                bump_dir = self.bump.bump_dir
                self.motion.move_backward()
                self.motion.rotate_away(bump_dir)

            elif self.is_boxed_out:
                # We're trapped, so rotate until not case anymore
                self.sound.make_beep()
                self.motion.rotate_tiny_bit(self.box_out_dir)

            else:  # Either approach target, or move to locate
                best_route = None, None, None
                is_frame_drop = False

                # Frame dropped, moving to locate
                if band_rect is None and still_homing:

                    best_route = self.last_best_route
                    is_frame_drop = True
                    self.num_frames_dropped += 1

                # We have actual target located
                elif band_rect is not None:

                    best_route = self.navigation.route_decide(
                        depth_image, target_bounding_box=band_rect, target_distance=band_dist, target_kind=None)

                    self.last_best_route = best_route
                    self.last_band_detect_time = cur_millis
                    is_frame_drop = False
                    self.num_frames_dropped = 0

                # The path decision
                best_horizontal, speed, found = best_route

                # Scan around
                if best_horizontal is None:

                    # Can't see anything - stop!
                    if (cur_millis - self.first_frame_drop_time) > 2000:
                        self.sound.make_beep()

                    # Spin in direction last calculated to be the 'best route'
                    b_h, _, _, = self.last_best_route
                    b_h = np.sign(b_h)
                    if b_h == 0:
                        b_h = 1
                    print "Scanning..."
                    self.motion.rotate_tiny_bit(-b_h)

                else:

                    ang_vel = -2 * best_horizontal
                    # ang_vel = (-0.5 * np.sign(best_horizontal) * abs(best_horizontal**0.5))

                    if is_frame_drop:

                        # We dropped target, but just for one frame
                        if self.first_frame_drop:
                            self.first_frame_drop_time = int(round(time.time() * 1000))
                            self.first_frame_drop = False

                        # Target lost for more than 10 frames -- instead of continuing best route, try turning a corner
                        if self.num_frames_dropped > 10:

                            # Calculate how far to move forward before turn
                            # Based on how far the last distance was
                            if self.frame_drop_motions_left == 0:
                                last_r, last_d = self.last_band_target
                                if last_d is not None:
                                    self.frame_drop_motions_left = int(math.ceil(last_d)) * 18 + 10

                            if self.frame_drop_motions_left > 8:

                                # Go forward to corner
                                best_h, speed, _ = 0, 0.5, 0
                                self.motion.move_forward(lin_vel=speed, ang_vel=-2*best_h)

                            else:
                                # Turn the corner
                                self.motion.move_forward(lin_vel=0, ang_vel=ang_vel)

                            self.frame_drop_motions_left -= 1

                        else:
                            # Target temporarily lost, just keep doing what you were doing
                            self.motion.move_forward(lin_vel=speed, ang_vel=ang_vel/2)

                    else:
                        # Target is well acquired, proceed to move toward
                        self.first_frame_drop = True
                        self.motion.move_forward(lin_vel=speed, ang_vel=ang_vel)

            # Not boxed out anymore
            if self.is_boxed_out and (cur_millis - self.last_box_out_time) > self.BOX_OUT_TIMEOUT_MS:
                self.is_boxed_out = False
                self.box_out_dir = None

            # wait for 0.1 seconds (10 HZ) and publish again
            r.sleep()

    def on_box_out(self, box_out_dir):

        if not self.is_boxed_out:
            # This is to keep a consistent turn direction when solving the boxed out problem
            self.box_out_dir = box_out_dir
            self.is_boxed_out = True
        self.last_box_out_time = int(round(time.time() * 1000))

    # Stop all activities
    def shutdown(self):

        # stop turtlebot
        self.is_shutdown = True
        rospy.loginfo("Stop TurtleBot")
        self.motion.stop_moving()
        # sleep just makes sure TurtleBot receives the stop command prior to shutting down the script
        rospy.sleep(5)


if __name__ == "__main__":

    bt = BandTracker()
    bt.run()
