import numpy as np
import cv2

from toolbox import CellStatus
from toolbox import Orientation

from graph_tools import PriorityQueue


class MapmakerLobe:

    """
    MapMaker handles the grid state, and also searching across the grid for the optimal next position.
    It can also display the map and take in updates to the grid state. It refers to poses in the manner:
    (row, col, Orientation), but is able to convert (x, y, theta) poses from NavigationLobe2 into this format
    (and back).

    See detemine_next_spot for info on the search algorithm.
    """

    def __init__(self):

        # Hard coded environment information
        map_buffer_space = 5  # space to add to grid sides
        og_map_w = 31 + (map_buffer_space * 2)
        og_map_h = 18 + (map_buffer_space * 2)

        # row, col, otn start position!
        self.start_pose = (8 + map_buffer_space, 30 + map_buffer_space, Orientation.West)

        self.started = False

        self.og_map = np.zeros((og_map_h, og_map_w))

        # start with all unknown
        for row in range(og_map_h):
            for col in range(og_map_w):
                self.set_cell_status(row, col, CellStatus.Unknown)

        # except for start spot duh, make these cells unoccupied
        start_row, start_col, _ = self.start_pose
        self.set_cell_status(start_row, start_col, CellStatus.Unoccupied)
        self.set_cell_status(start_row + 1, start_col, CellStatus.Unoccupied)
        self.set_cell_status(start_row, start_col + 1, CellStatus.Unoccupied)
        self.set_cell_status(start_row + 1, start_col + 1, CellStatus.Unoccupied)

        # keep track of the last pose that was found in the search
        self.last_target_pose = self.start_pose

    def start(self):

        self.started = True

    def nav_to_grid_pose(self, nav_pose):

        """

        Converts (x, y, theta) odometry pose readings to (row, col, Orientation) pose format.

        :param nav_pose:
        :return:
        """

        x, y, theta = nav_pose

        # Convert from 0.2m to 1 unit
        x *= 5
        y *= 5

        start_row, start_col, start_otn = self.start_pose

        if start_otn is Orientation.West:
            row = start_row + y
            col = start_col - x
            front, back, left, right = Orientation.West, Orientation.East, Orientation.South, Orientation.North

        elif start_otn is Orientation.East:
            row = start_row + y
            col = start_col + x
            front, back, left, right = Orientation.East, Orientation.West, Orientation.North, Orientation.South

        elif start_otn is Orientation.North:
            row = start_row - x
            col = start_col - y
            front, back, left, right = Orientation.North, Orientation.South, Orientation.West, Orientation.East

        elif start_otn is Orientation.South:
            row = start_row + x
            col = start_col - y
            front, back, left, right = Orientation.South, Orientation.North, Orientation.East, Orientation.West

        ang_err = 3

        if abs(theta) < 0 + ang_err:
            otn = front

        elif (-90 + ang_err) > theta > (-90 - ang_err):
            otn = right

        elif (90 - ang_err) < theta < (90 + ang_err):
            otn = left

        elif theta > (180 - ang_err) or theta < (-180 + ang_err):
            otn = back

        row = int(round(row))
        col = int(round(col))

        return row, col, otn

    def grid_to_nav_pose(self, grid_pose):

        """

        Converts (row, col, Orientation) grid pose format to (x, y, theta) odometry pose reading
        format.

        :param grid_pose:
        :return:
        """

        row, col, otn = grid_pose

        start_row, start_col, start_otn = self.start_pose

        if start_otn is Orientation.West:
            x = start_col - col
            y = row - start_row
            front, back, left, right = Orientation.West, Orientation.East, Orientation.South, Orientation.North

        elif start_otn is Orientation.East:
            x = col - start_col
            y = row - start_row
            front, back, left, right = Orientation.East, Orientation.West, Orientation.North, Orientation.South

        elif start_otn is Orientation.North:
            x = start_row - row
            y = start_col - col
            front, back, left, right = Orientation.North, Orientation.South, Orientation.West, Orientation.East

        elif start_otn is Orientation.South:
            x = row - start_row
            y = start_col - col
            front, back, left, right = Orientation.South, Orientation.North, Orientation.East, Orientation.West

        if otn is front:
            theta = 0
        elif otn is back:
            theta = 180
        elif otn is left:
            theta = 90
        elif otn is right:
            theta = -90

        # 1 unit is 0.2m
        x *= 0.2
        y *= 0.2

        return x, y, theta

    @staticmethod
    def status_to_num(status):

        """
        Wrapper method for hiding grid values (ints) under enums
        :param status:
        :return:
        """

        if status is CellStatus.Occupied:
            return 1
        elif status is CellStatus.Unoccupied:
            return 0
        elif status is CellStatus.Unknown:
            return -1

    @staticmethod
    def num_to_status(num):

        """
        Wrapper method for hiding grid values (ints) under enums
        :param num:
        :return:
        """
        if num == 1:
            return CellStatus.Occupied
        elif num == 0:
            return CellStatus.Unoccupied
        elif num == -1:
            return CellStatus.Unknown

    def get_cell_status(self, row, col):

        """
        Returns CellStatus.Occupied, Unknown, Unoccupied for cell's status on grid
        """
        return self.num_to_status(self.og_map[row][col])

    def set_cell_status(self, row, col, status):

        """
        Updates cell status to CellStatus passed in.
        """
        self.og_map[row][col] = self.status_to_num(status)

    def determine_next_spot(self, start_pose):

        """
        Determines the next unoccupied spot to travel to based on
        number of turns required to get there and route distance.

        Interesting part of algorithm in search_pose_list method!

        :param start_pose: A tuple containing: (x, y, Theta) top left
        :return: A list containing the intermediate poses required to travel to
        the destination pose selected by the search. Empty if there is no
        spot to travel to, i.e., the map is complete.
        """

        start_pose = self.nav_to_grid_pose(start_pose)

        # Look into search_pose_list, this is where magic is happening!
        grid_pose_list = self.search_pose_list(start_pose)

        # Convert to nav format and get rid of poses that are stops along a straight line route
        nav_pose_list = []

        for grid_pose in grid_pose_list:

            nav_pose = self.grid_to_nav_pose(grid_pose)

            # get rid of poses that are stops along a straight line route, redundant
            if len(nav_pose_list) > 1:
                last_nav_pose = nav_pose_list[-1]
                l_x, l_y, l_theta = last_nav_pose
                c_x, c_y, c_theta = nav_pose

                # if straight lining
                if abs(l_theta - c_theta) < 1 and (abs(l_x - c_x) < 0.01 or abs(l_y - c_y) < 0.01):
                    nav_pose_list.pop()

            nav_pose_list.append(nav_pose)

        return nav_pose_list

    def search_pose_list(self, start_pose):

        """

        Determines the next unoccupied spot to travel to based on
        number of turns required to get there and route distance.

        Dijkstra's with number of turns and path distance as cost.
        Neighbor node are spots that are either one turn away, or one movement forward/backward away.
        For example, any pose that is the same coordinate, just with a different orientation, or
        a pose that is the same orientation but one cell forward or one cell backward.
        Cells are selected to be "frontier" cells if they are unoccupied but border an unknown cell.

        :param start_pose: A tuple containing: (row, col, Orientation) top left
        :return: A list containing the intermediate poses required to travel to
        the destination pose selected by the search. Empty if there is no
        spot to travel to, i.e., the map is complete.
        """

        # Queue to measure cost of traveling to cell
        pose_cost = PriorityQueue()

        # Dictionary of cost
        cost = {}
        # Used to find paths in end, once search is complete
        prev_pose = {}
        # No repeats!
        visited = set()

        # Insert the start pose as the seed
        pose_cost.insert(start_pose, 0)
        cost[start_pose] = 0
        prev_pose[start_pose] = None

        # Our best frontier pose so far, and its cost
        best_frontier_pose = None
        best_frontier_cost = float("inf")

        # Pop the queue
        while not pose_cost.is_empty():

            # Data on current pose
            cur_cost, cur_pose = pose_cost.pop()
            pose_row, pose_col, pose_otn = cur_pose
            visited.add(cur_pose)

            # Keep frontiers updated NEW NEW NEW
            if self.is_pose_frontier(cur_pose):
                if cur_cost < best_frontier_cost:
                    best_frontier_pose = cur_pose
                    best_frontier_cost = cur_cost

            # Check neighbors -- poses that are one motion (forward, backward) or rotation from current
            neighbors = set()
            if pose_otn is Orientation.North:
                neighbors.add((pose_row - 1, pose_col, pose_otn))  # forward
                neighbors.add((pose_row + 1, pose_col, pose_otn))  # backward
                neighbors.add((pose_row, pose_col, Orientation.West))  # west
                neighbors.add((pose_row, pose_col, Orientation.East))  # east
                neighbors.add((pose_row, pose_col, Orientation.South))  # south
            if pose_otn is Orientation.South:
                neighbors.add((pose_row - 1, pose_col, pose_otn))  # backward
                neighbors.add((pose_row + 1, pose_col, pose_otn))  # forward
                neighbors.add((pose_row, pose_col, Orientation.West))  # west
                neighbors.add((pose_row, pose_col, Orientation.East))  # east
                neighbors.add((pose_row, pose_col, Orientation.North))  # north
            if pose_otn is Orientation.East:
                neighbors.add((pose_row, pose_col + 1, pose_otn))  # forward
                neighbors.add((pose_row, pose_col - 1, pose_otn))  # backward
                neighbors.add((pose_row, pose_col, Orientation.West))  # west
                neighbors.add((pose_row, pose_col, Orientation.North))  # north
                neighbors.add((pose_row, pose_col, Orientation.South))  # south
            if pose_otn is Orientation.West:
                neighbors.add((pose_row, pose_col - 1, pose_otn))  # forward
                neighbors.add((pose_row, pose_col + 1, pose_otn))  # backward
                neighbors.add((pose_row, pose_col, Orientation.North))  # west
                neighbors.add((pose_row, pose_col, Orientation.East))  # east
                neighbors.add((pose_row, pose_col, Orientation.South))  # south

            map_h, map_w = self.og_map.shape
            # remove neighbors that are not window sized or outside bounds of grid
            to_remove = set()
            for n_pose in neighbors:
                n_row, n_col, n_otn = n_pose

                """"
                # check if this neighbor is an unknown pose
                if self.get_cell_status(n_row, n_col) is CellStatus.Unknown:
                    # Then this cur node is a frontier pose
                    if cur_cost < best_frontier_cost:
                        best_frontier_pose = cur_pose
                        best_frontier_cost = cur_cost
                """

                if n_pose in visited:
                    to_remove.add(n_pose)
                    continue

                # check bounds on grid
                if n_row < 0 or n_row >= map_h or n_col < 0 or n_col >= map_w:
                    to_remove.add(n_pose)
                    continue

                # check turtlebot fits in this cell! (2x2 space)
                fits = True
                fits = fits and (self.get_cell_status(n_row, n_col) is CellStatus.Unoccupied)
                fits = fits and (self.get_cell_status(n_row + 1, n_col) is CellStatus.Unoccupied)
                fits = fits and (self.get_cell_status(n_row, n_col + 1) is CellStatus.Unoccupied)
                fits = fits and (self.get_cell_status(n_row + 1, n_col + 1) is CellStatus.Unoccupied)

                if not fits:
                    to_remove.add(n_pose)

            for n_pose in to_remove:
                neighbors.remove(n_pose)

            # now update remaining neighbors into queue!

            for n_pose in neighbors:

                # We may have a lower/better cost to this neighbor pose
                if n_pose in cost:
                    old_cost = cost[n_pose]
                else:
                    old_cost = float("inf")

                # Figure out how much to add to current cost
                _, _, n_otn = n_pose
                if not n_otn == pose_otn:
                    cost_addition = 1  # Turns are not preferred over straight line movements
                else:
                    cost_addition = 0  # Straight line movements preferred over turns

                new_cost = cur_cost + cost_addition

                # Update to the better cost
                if new_cost < old_cost:

                    cost[n_pose] = new_cost
                    prev_pose[n_pose] = cur_pose
                    pose_cost.insert(n_pose, new_cost)

        # end while, queue popping

        # get path to lowest cost frontier
        pose_list = []

        # Plod back from the destination pose using prev_pose
        cur_pose = best_frontier_pose

        self.last_target_pose = cur_pose

        while cur_pose is not None:

            pose_list.append(cur_pose)
            cur_pose = prev_pose[cur_pose]

        # reverse it so that the last pose is last in list!
        pose_list = list(reversed(pose_list))

        return pose_list

    def is_pose_frontier(self, grid_pose):

        """
        Given (row, col, Orientation), tell if this is a pose that has at least one unknown in front of it
        """
        l_row, l_col = 0, 0
        r_row, r_col = 0, 0

        row, col, otn = grid_pose

        if otn is Orientation.North:
            l_row, l_col = row - 1, col
            r_row, r_col = row - 1, col + 1
        elif otn is Orientation.South:
            l_row, l_col = row + 2, col + 1
            r_row, r_col = row - 2, col
        elif otn is Orientation.West:
            l_row, l_col = row + 1, col - 1
            r_row, r_col = row, col - 1
        elif otn is Orientation.East:
            l_row, l_col = row, col + 2
            r_row, r_col = row + 1, col + 2

        is_left_unknown = self.get_cell_status(l_row, l_col) is CellStatus.Unknown
        is_right_unknown = self.get_cell_status(r_row, r_col) is CellStatus.Unknown

        return is_left_unknown or is_right_unknown

    def map_update(self, current_pose, left_front, right_front):

        """

        Updates the OG map with new occupancy data.

        1.0 or 0.0, for occupied or nah

        :param current_pose: A tuple containing: (x, y, theta) top left

        """

        grid_pose = self.nav_to_grid_pose(current_pose)
        row, col, otn = grid_pose

        if otn is Orientation.West:
            l_row, l_col = row + 1, col - 1
            r_row, r_col = row, col - 1
        elif otn is Orientation.East:
            l_row, l_col = row, col + 2
            r_row, r_col = row + 1, col + 2
        elif otn is Orientation.North:
            l_row, l_col = row - 1, col
            r_row, r_col = row - 1, col + 1
        elif otn is Orientation.South:
            l_row, l_col = row + 2, col
            r_row, r_col = row + 2, col + 1

        if abs(left_front - 1.0) < 0.1:
            self.set_cell_status(l_row, l_col, CellStatus.Occupied)
        elif abs(left_front - 0.0) < 0.1:
            self.set_cell_status(l_row, l_col, CellStatus.Unoccupied)

        if abs(right_front - 1.0) < 0.1:
            self.set_cell_status(r_row, r_col, CellStatus.Occupied)
        elif abs(right_front - 0.0) < 0.1:
            self.set_cell_status(r_row, r_col, CellStatus.Unoccupied)

    def display_map_nav(self, current_pose):

        self.display_map(self.nav_to_grid_pose(current_pose))

    def display_map(self, current_pose):

        """

        Draws a map to specification using (row, col, Orientation) as current pose info

        """
        map_h, map_w = self.og_map.shape

        # draw grid of squares
        square_size = 20
        im_h = map_h * square_size
        im_w = map_w * square_size

        disp_image = np.ones((im_h, im_w, 3), np.uint8)
        disp_image *= 255

        # Colors to use
        unocc_color = (255, 255, 255)
        occ_color = (0, 0, 0)
        unknown_color = (255, 0, 0)

        for row in range(map_h):
            for col in range(map_w):

                pt1x, pt1y = col * square_size, row * square_size
                pt2x, pt2y = pt1x + square_size, pt1y + square_size

                color = (0, 255, 0)
                cell_status = self.get_cell_status(row, col)
                if cell_status is CellStatus.Occupied:
                    color = occ_color
                elif cell_status is CellStatus.Unoccupied:
                    color = unocc_color
                elif cell_status is CellStatus.Unknown:
                    color = unknown_color

                # Draw colored rectangle
                cv2.rectangle(disp_image, (pt1x, pt1y), (pt2x, pt2y), color, thickness=-1)

                cv2.rectangle(disp_image, (pt1x, pt1y), (pt2x, pt2y), (0, 0, 0), thickness=1)

        # draw robot position as thick black border

        cur_row, cur_col, cur_otn = current_pose

        pt1x, pt1y = cur_col * square_size, cur_row * square_size
        pt2x, pt2y = pt1x + square_size*2, pt1y + square_size*2

        cv2.rectangle(disp_image, (pt1x, pt1y), (pt2x, pt2y), (0, 0, 0), thickness=3)

        # Indicate robot orientation with magenta circle on square side

        if cur_otn is Orientation.West:
            a_pt2 = pt1x, pt1y + square_size

        elif cur_otn is Orientation.East:
            a_pt2 = pt2x, pt1y + square_size

        elif cur_otn is Orientation.North:
            a_pt2 = pt1x + square_size, pt1y

        elif cur_otn is Orientation.South:
            a_pt2 = pt1x + square_size, pt2y

        cv2.circle(disp_image, a_pt2, 5, (200, 0, 200), thickness=-1)

        # Draw the current target with cyan box

        tgt_row, tgt_col, tgt_otn = self.last_target_pose
        pt1x, pt1y = tgt_col * square_size, tgt_row * square_size
        pt2x, pt2y = pt1x + square_size*2, pt1y + square_size*2
        if tgt_otn is Orientation.West:
            a_pt2 = pt1x, pt1y + square_size

        elif tgt_otn is Orientation.East:
            a_pt2 = pt2x, pt1y + square_size

        elif tgt_otn is Orientation.North:
            a_pt2 = pt1x + square_size, pt1y

        elif tgt_otn is Orientation.South:
            a_pt2 = pt1x + square_size, pt2y

        cv2.rectangle(disp_image, (pt1x, pt1y), (pt2x, pt2y), (200, 200, 0), thickness=2)
        cv2.circle(disp_image, a_pt2, 5, (200, 200, 0), thickness=2)

        # Show and save image
        cv2.imshow("Marauder's Map", disp_image)
        cv2.imwrite("last_map.png", disp_image)
        cv2.waitKey(3)

    def print_nav_pose_list_as_grid_list(self, nav_pose_list):

        """
        Print the pose list in the grid (row, col, Orientation) format
        when given (x, y, theta) format.
        """

        grid_pose_list = []
        for nav_pose in nav_pose_list:
            grid_pose_list.append(self.nav_to_grid_pose(nav_pose))

        print "***POSE LIST***"
        for pose in grid_pose_list:

            row, col, otn = pose
            print "%d %d %s" % (row, col, otn)
        print "***   **    ***"





