from toolbox import Orientation
import toolbox

import numpy as np
import math


class NavigationLobe2:

    """
    NavigationLobe handles the actual pose to pose movement, where it turns to the destination,
    travels the path while correcting curves, and turns to the final pose angle.

    To do this well, it takes in odometry data and uses a feedback loop to adjust its linear and
    angular velocities in conjunction with MotionManager.
    """

    def __init__(self):

        self.started = False
        self.motion = None
        self.odometry = None

        self.max_lin_speed = 0.2  # straight line speed
        self.closeness_threshold_lin = 0.1  # how long to ease in/ease out for
        self.closeness_proportion_lin = 0.5  # ease in/ease out factor
        self.closeness_min_lin = 0.05  # minimum linear speed

        self.max_ang_speed = 0.5  # turn angle speed
        self.max_ang_speed_in_curve = 0.3  # turn angle speed when trying to go straight (correcting)
        self.closeness_threshold_ang = 0.1  # how long to ease in/ease out for
        self.closeness_proportion_ang = 0.5  # ease in/ease out factor
        self.closeness_min_ang = 0.025  # minimum angular speed

        self.ang_error_thresh = 0.5  # ang error to begin reacting to
        self.lin_error_thresh = 0.1  # lin error to begin reacting to

        self.start_odom_pose = None  # The pose it started it during a travel
        self.start_curve_theta = 0  # The angle it started moving forward in to destination
        self.end_odom_pose = None  # The end pose to reach

        self.turn_angle_start = 0  # Used in turn_angle method to turn from and to
        self.turn_angle_end = 0  # Used in turn_angle method to turn from and to

        self.is_moving = False

    def start(self, motion, odometry):

        self.motion = motion
        self.odometry = odometry
        self.started = True

        # Used because otherwise robot is stuck....
        self.motion.clear_nudge()

    def get_current_pose(self):

        """
        Polls odometry
        """

        return self.odometry.x, self.odometry.y, self.odometry.theta

    def travel_to_spot(self, pose_list):

        """
        Given a pose list, it executes the movements specified.
        """

        for i in range(len(pose_list)):

            if i > 0:
                last_pose = pose_list[i - 1]
                cur_pose = pose_list[i]

                l_x, l_y, l_theta = last_pose
                c_x, c_y, c_theta = cur_pose

                # If next pose is just an angle difference, use turn_angle function instead.
                if abs(l_x - c_x) < 0.01 and abs(l_y - c_y) < 0.01:

                    self.turn_angle(c_theta)

                else:

                    self.GOTO_turns(cur_pose)

            else:
                self.GOTO_turns(pose_list[i])

    def GOTO_turns(self, end_pose):

        """
        Will move to (x, y, theta) in end_pose as described at top of class.
        """

        if self.is_moving:
            print "ERR: Already moving..."
            return

        self.is_moving = True

        cur_x, cur_y, cur_theta = (self.odometry.x, self.odometry.y, self.odometry.theta)
        end_x, end_y, end_theta = end_pose
        path_theta = math.degrees(np.arctan2((end_y - cur_y),(end_x - cur_x)))
        # MOVE!

        # Face destination first
        self.turn_angle(path_theta)

        cur_x, cur_y, cur_theta = (self.odometry.x, self.odometry.y, self.odometry.theta)
        path_theta = math.degrees(np.arctan2((end_y - cur_y),(end_x - cur_x)))
        curve_end_pose = (end_x, end_y, path_theta)

        # Move to destination
        self.curve_path(curve_end_pose)

        # Set final angle
        self.turn_angle(end_theta)

        self.is_moving = False

    def turn_angle(self, end_theta):

        """
        Turns to an absolute world angle from current angle.
        """

        self.turn_angle_start = self.odometry.theta
        self.turn_angle_end = end_theta
        self.motion.move_rate(self.feedback_turn)

    def curve_path(self, end_pose):

        """
        Travels to (x, y, theta) = end_pose as a straight as possible while correcting drift.
        """

        self.start_odom_pose = (self.odometry.x, self.odometry.y, self.odometry.theta)
        self.end_odom_pose = end_pose

        self.start_curve_theta = self.odometry.theta
        self.motion.move_rate(self.feedback_curve)

    def feedback_turn(self):

        """
        Velocity feedback handler for turning an angle -- eases in, goes uniform vel, then ease out
        """

        _, _, cur_theta = (self.odometry.x, self.odometry.y, self.odometry.theta)

        lin_speed = 0  # Just starting to turn for now
        ang_vel = self.calculate_angular_velocity(self.turn_angle_start, cur_theta, self.turn_angle_end)

        ang_dist = self.normalize_angle(self.turn_angle_end - cur_theta)

        halt = (abs(ang_dist) < self.ang_error_thresh)

        return lin_speed, ang_vel, halt

    def feedback_curve(self):

        """
        Velocity feedback for moving down a path, including angular correction and easing in/out of lin speed
        """

        start_x, start_y, start_theta = self.start_odom_pose
        cur_x, cur_y, cur_theta = (self.odometry.x, self.odometry.y, self.odometry.theta)
        end_x, end_y, end_theta = self.end_odom_pose

        lin_speed = self.calculate_linear_velocity((start_x, start_y), (cur_x, cur_y), (end_x, end_y))

        ang_vel = self.calculate_angular_velocity_for_curve(start_theta, cur_theta, end_theta)

        lin_dist = np.sqrt((cur_x - end_x)**2 + (cur_y - end_y)**2)

        halt = lin_dist < self.lin_error_thresh

        return lin_speed, ang_vel, halt

    def calculate_linear_velocity(self, start_pos, cur_pos, end_pos):

        """
        Calculates what the new speed should be based on dest and source,
        as well as the current intermediate pose
        """

        start_x, start_y = start_pos
        cur_x, cur_y = cur_pos
        end_x, end_y = end_pos

        dist_traveled = np.sqrt((cur_x - start_x)**2 + (cur_y - start_y)**2)
        dist_from_dst = np.sqrt((cur_x - end_x)**2 + (cur_y - end_y)**2)

        # Uniform speed
        lin_speed = self.max_lin_speed

        if dist_traveled < self.closeness_threshold_lin:
            # Ease in
            lin_speed = (self.closeness_proportion_lin * dist_traveled) + self.closeness_min_lin

        if dist_from_dst < self.closeness_threshold_lin:
            # Ease out
            lin_speed = (self.closeness_proportion_lin * dist_from_dst) + self.closeness_min_lin

        return lin_speed

    def calculate_angular_velocity(self, start_theta, cur_theta, end_theta):

        """
        Calculates what the new speed should be based on dest and source,
        as well as the current intermediate pose
        """

        ang_traveled = self.normalize_angle(cur_theta - start_theta)
        ang_from_dst = self.normalize_angle(end_theta - cur_theta)

        # Uniform velocity
        ang_vel = np.sign(ang_from_dst) * self.max_ang_speed

        if abs(ang_traveled) < self.closeness_threshold_ang:
            # Ease in
            ang_vel = (self.closeness_proportion_ang * ang_traveled) + np.sign(ang_from_dst) * self.closeness_min_ang

        if abs(ang_from_dst) < self.closeness_threshold_ang:
            # Ease out
            ang_vel = (self.closeness_proportion_ang * ang_from_dst) + np.sign(ang_from_dst) * self.closeness_min_ang

        return ang_vel

    def calculate_angular_velocity_for_curve(self, start_theta, cur_theta, end_theta):

        """
        Same as calculate_angular_velocity, but function is adjusted to be less
        jittery for doing angular corrections on path movements.
        """

        ang_traveled = self.normalize_angle(cur_theta - start_theta)
        ang_from_dst = self.normalize_angle(end_theta - cur_theta)

        ang_vel = np.sign(ang_from_dst) * self.max_ang_speed_in_curve

        if abs(ang_traveled) < self.closeness_threshold_ang:

            ang_vel = (self.closeness_proportion_ang * ang_traveled)

        if abs(ang_from_dst) < self.closeness_threshold_ang:

            ang_vel = (self.closeness_proportion_ang * ang_from_dst)

        return ang_vel

    @staticmethod
    def normalize_angle(angle):

        """
        Correct theta to -180 to 180
        """

        if angle <= -180:
            angle += 360
        elif angle > 180:
            angle -= 360

        return angle
