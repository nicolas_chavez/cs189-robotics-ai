
import rospy
from nav_msgs.msg import Odometry
import math
import numpy as np


class OdomManager:

    """


    """

    def __init__(self):

        self.started = False

        self.initial_captured = False
        self.initial_x = 0
        self.initial_y = 0
        self.initial_theta = 0

        self.x = 0
        self.y = 0
        self.theta = 0

    def start(self):

        rospy.Subscriber('/odom', Odometry, self.on_odometry)
        # rospy.Subscriber('/mobile_base/sensors/imu_data', )
        self.started = True

    def transform_values_initial(self, x, y, theta):

        # Transform incoming odometry such that it is in a coordinate frame corresponding to the robot's initial pose
        # subtract translation vector and then perform rotation in reverse direction

        initial_vec = np.array([x, y])
        initial_vec -= np.array([self.initial_x, self.initial_y])
        rot_ang = -math.radians(self.initial_theta)
        rot_mat = np.array([[np.cos(rot_ang), -np.sin(rot_ang)], [np.sin(rot_ang), np.cos(rot_ang)]])
        transformed_vec = rot_mat.dot(initial_vec)

        x = transformed_vec[0]
        y = transformed_vec[1]

        theta -= self.initial_theta

        # correct theta to -180 to 180
        if theta <= -180:
            theta += 360
        elif theta > 180:
            theta -= 360

        return x, y, theta

    def on_odometry(self, data):

        # get odometery position and orientations
        pose = data.pose.pose
        position = pose.position
        orientation = pose.orientation

        # extract relevant values
        z, w = orientation.z, orientation.w
        x, y = position.x, position.y

        # calulate single theta to convert from quaternion
        theta = 2 * math.degrees(np.arctan2(z, w))

        # if not the first odom frame 
        if self.initial_captured:

            self.x, self.y, self.theta = self.transform_values_initial(x, y, theta)

        # if it's the first odom frame
        else:

            self.initial_x = x
            self.initial_y = y
            self.initial_theta = theta
            self.initial_captured = True
            self.x = 0
            self.y = 0
            self.theta = 0

        #print "x: %f y: %f theta: %f" % (self.x, self.y, self.theta)


