import rospy
import numpy as np
import time
import math
from toolbox import Orientation

from OutputManagers.sound_manager import SoundManager
from OutputManagers.motion_manager import MotionManager

from SensorManagers.bump_manager import BumpManager
from SensorManagers.cliff_manager import CliffManager
from SensorManagers.depth_manager import DepthManager
from SensorManagers.wheel_drop_manager import WheelDropManager
from SensorManagers.camera_manager import CameraManager
from SensorManagers.odom_manager import OdomManager

from Brainz.navigation_lobe2 import NavigationLobe2
from Brainz.mapmaker_lobe import MapmakerLobe
from Brainz.obstacle_lobe import ObstacleLobe


class PadMapper:

    """

    PadMapper is the main class to run to kick off the mapping procedure. PadMapper's strategy in
    described in detail in the main_loop method. It handles controlling and feeding info back
    and forth from the different modules.

    """

    def __init__(self):

        self.is_shutdown = False

        # Instantiate different managers

        self.motion = MotionManager()
        self.sound = SoundManager()

        self.camera = CameraManager()
        self.bump = BumpManager()
        self.cliff = CliffManager()
        self.depth = DepthManager()
        self.wheel_drop = WheelDropManager()
        self.odometry = OdomManager()

        self.mapmaker = MapmakerLobe()
        self.navigation = NavigationLobe2()
        self.obstacle = ObstacleLobe()

        # Dummy null value
        self.current_pose = (-100, -100, Orientation.West)

    def run(self):

        """
        Starts up the different modules and kicks off main loop.
        """

        # Initialization

        rospy.init_node('BandTracker', anonymous=False)
        rospy.on_shutdown(self.shutdown)

        # Start modules

        self.motion.start()
        self.sound.start()

        self.camera.start()
        self.bump.start()
        self.cliff.start()
        self.depth.start()
        self.wheel_drop.start()
        self.odometry.start()

        self.mapmaker.start()

        self.navigation.start(self.motion, self.odometry)
        self.navigation.current_pose = self.current_pose

        self.obstacle.start(self.depth, self.motion)

        # shutdown if wheel dropped
        self.wheel_drop.on_drop_callback = self.shutdown

        # first is forward, second is horizontal, angle is 180 to -180, left to right

        # as long as you haven't shutdown keeping doing...

        self.main_loop()

    def main_loop(self):

        """
        The strategy is the following:
            1. Determine the next best pose to go to using the map.
            2. Check if this is None, which means we've finished mapping.
            3. Else, move to this pose by executing the pose list returned
            4. Scan the two spaces in front of using the depth camera
            5. Update the map with this information
            6. Repeat.
        """

        while not self.is_shutdown:

            # Determine next spot

            self.current_pose = self.navigation.get_current_pose()

            self.mapmaker.display_map_nav(self.current_pose)

            # 1. Determine the next best pose to go to using the map.
            pose_list = self.mapmaker.determine_next_spot(self.current_pose)

            # 2. Check if this is None, which means we've finished mapping.
            if len(pose_list) == 0:
                break  # we explored room!

            pose_list = pose_list[1:]
            self.mapmaker.print_nav_pose_list_as_grid_list(pose_list)
            # Travel to destination
            # 3. Else, move to this pose by executing the pose list returned
            self.navigation.travel_to_spot(pose_list)

            # Update current pose
            self.current_pose = self.navigation.get_current_pose()

            # Update depth feed
            depth_image = self.depth.current_image

            # 4. Scan the two spaces in front of using the depth camera
            depth_image = self.depth.binarize_depth_image(depth_image)
            presences = self.obstacle.scan(depth_image)

            print presences

            # self.depth.draw_image_and_obstacles(255*depth_image, None)

            # 5. Update the map with this information
            self.mapmaker.map_update(self.current_pose, presences[0], presences[1])

    def shutdown(self):

        """
        Stops the turtlebot's motion completely.
        """

        self.is_shutdown = True
        rospy.loginfo("Stop TurtleBot")
        self.motion.stop_moving()
        # sleep just makes sure TurtleBot receives the stop command prior to shutting down the script
        rospy.sleep(5)

if __name__ == "__main__":

    pm = PadMapper()
    pm.run()
