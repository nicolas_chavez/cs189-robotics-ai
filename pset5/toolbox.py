from enum import Enum
from enum import IntEnum


class Orientation(IntEnum):

    """
    Used to discretize the orientation space, safe with enums
    """

    North = 0
    South = 1
    West = 2
    East = 3

# Used as grid cell values
CellStatus = Enum('CellStatus', 'Occupied Unoccupied Unknown')
